class Config {
  String key; //KEY FOR ENCRYPT | MD5

  //FOR API
  String ipmain;
  String getslidelist;
  String getpromolist;
  String getpaketlist;
  String getproductlistlimit;
  String getproductlistcategory;
  String getproductlistcategorysub;
  String getcategorylist;
  String getcategorysublist;
  String cartinsert;
  String cartlist;
  String cartdelete;

  //AUTH
  String authcheck;
  String authget;
  String authregister;

  //FOR API MAGENTO
  String mgpathimage;
  String mggetcatalogall;
  String mggetcatalogcategory;
  String mggetcatalogcategorysub;
  String mgcartlist;
  String mgcartinsert;
  String mgcartdelete;

  //FOR SHARE PREF
  String prefcurrency;

  //PREFERENCE DATA MEMBER
  String prefmemberkeylog; //GENERATE ID FIREBASE + PHONE NUMBER
  String prefmembernomorhp;
  String prefmembernamadepan;
  String prefmembernamabelakang;
  String prefmembertgllahir;
  String prefmemberphotoidcard;
  String prefmemberfacebookname;
  String prefmemberfacebookemail;
  String prefmemberfacebookphone;
  String prefmemberfacebookphoto;
  String
      prefmemberlocationurl; //https://www.google.co.id/maps/@-7.8274406,112.0102002,20z
  String prefmemberlocationfull; //Jl. Raden Patah, Gg. Slamet, No. 02

  Config({
    this.key =
        '38bab1e72d0e9d1d093a476c7ef4fd81', //key for generate encrypt in Authentication | in Evernote

    this.ipmain = "https://m.arafah.hk/",
    //this.ipmain = "http://192.168.100.2/arafah/arafahm/",
    this.getslidelist = "api/slide",
    this.getpromolist = "api/promo",
    this.getpaketlist = "api/paket",
    this.getproductlistlimit = "api/product/limit",
    this.getproductlistcategory = "api/product/category/", //'id_categories'
    this.getproductlistcategorysub =
        "api/product/categorysub/", //'id_categories/id_categorysub'
    this.getcategorylist = "api/category",
    this.getcategorysublist = "api/category/sub/", //id_categories
    this.cartlist = 'api/cart',
    this.cartinsert = 'api/cart/insert',
    this.cartdelete = 'api/cart/delete/', //'id_cart'

    this.authcheck = 'api/auth/check', //check apakah ada atau tidak
    this.authget = 'api/auth/get', //get all data
    this.authregister = 'api/auth/register', //insert data

    this.mgpathimage = "https://www.arafah.hk/pub/media/catalog/product",
    this.mggetcatalogall = "api_mg/catalog/all",
    this.mggetcatalogcategory = "api_mg/catalog/category",
    this.mggetcatalogcategorysub = "api_mg/catalog/categorysub",
    this.mgcartlist = "api_mg/cart",
    this.mgcartinsert = "api_mg/cart/insert",
    this.mgcartdelete = "api_mg/cart/delete/", //'id_cart'

    this.prefcurrency = 'currency',
    this.prefmemberkeylog =
        'memberkeylog', //GENERATE ID FIREBASE + PHONE NUMBER
    this.prefmembernomorhp = 'membernomorhp',
    this.prefmembernamadepan = 'membernamadepan',
    this.prefmembernamabelakang = 'membernamabelakang',
    this.prefmembertgllahir = 'membertgllahir',
    this.prefmemberphotoidcard = 'memberphotoidcard',
    this.prefmemberfacebookname = 'memberfacebookname',
    this.prefmemberfacebookemail = 'memberfacebookemail',
    this.prefmemberfacebookphone = 'memberfacebookphone',
    this.prefmemberfacebookphoto = 'memberfacebookphoto',
    this.prefmemberlocationurl = 'memberlocationurl',
    this.prefmemberlocationfull = 'memberlocationfull',
  });
}
