import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

//AMBIL DATA MEMBER | PAGE USER MAIN
class AuthenticationGet {
  String idmember;
  String membergenerateid;
  String memberphone;
  String membernamadepan;
  String membernamabelakang;
  String membertgllahir;
  //String memberphotoidcard;
  String memberfacebookname;
  String memberfacebookemail;
  //String memberfacebookphone;
  String memberfacebookphoto;
  // String memberlocationurl;
  // String memberlocationfull;
  String createdat;
  String updatedat;

  AuthenticationGet(
      {this.idmember,
      this.membergenerateid,
      this.memberphone,
      this.membernamadepan,
      this.membernamabelakang,
      this.membertgllahir,
      //this.memberphotoidcard,
      this.memberfacebookname,
      this.memberfacebookemail,
      //this.memberfacebookphone,
      this.memberfacebookphoto,
      // this.memberlocationurl,
      // this.memberlocationfull,
      this.createdat,
      this.updatedat});

  factory AuthenticationGet.getAuth(Map<String, dynamic> object) {
    return AuthenticationGet(
      idmember: object['id_member'],
      membergenerateid: object['member_generate_id'],
      memberphone: object['member_phone'],
      membernamadepan: object['member_nama_depan'],
      membernamabelakang: object['member_nama_belakang'],
      membertgllahir: object['member_tgl_lahir'],
      //memberphotoidcard: object['member_photo_id_card'],
      memberfacebookname: object['member_fb_name'],
      memberfacebookemail: object['member_fb_email'],
      //memberfacebookphone: object['member_fb_phone'],
      memberfacebookphoto: object['member_fb_photo'],
      //memberlocationurl: object['member_location_url'],
      //memberlocationfull: object['member_location_full'],
      createdat: object['created_at'],
      updatedat: object['updated_at'],
    );
  }

  static Future<AuthenticationGet> postGetAuthFromAPI(String key) async {
    String apiURL = Config().ipmain + Config().authget;

    //String test = '5ibJiNXmP0QvCVvuLvGXbEYDBAD3+space++85298765432';
    //print(key);
    var arrayKey = key.split("+space+");
    var apiResult = await http.post(apiURL, body: {
      "member_generate_id": arrayKey[0],
      "member_phone": arrayKey[1],
    });
    var jsonObject = json.decode(apiResult.body);

    return AuthenticationGet.getAuth(jsonObject);
  }
}

class UninitializedAuthenticationGet extends AuthenticationGet {}

//===========================================================================
//INSERT DATA MEMBER | SETELAH INPUT OTP
class AuthenticationRegister {
  static Future<AuthenticationRegister> postRegisterAuthFromAPI(
      String key) async {
    String apiURL = Config().ipmain + Config().authregister;

    var arrayKey = key.split("+space+");
    await http.post(apiURL, body: {
      "member_generate_id": arrayKey[0],
      "member_phone": arrayKey[1],
      "member_nama_depan": arrayKey[2],
      "member_nama_belakang": arrayKey[3],
      "member_tgl_lahir": arrayKey[4],
      "member_photo_id_card": arrayKey[5],
      "member_fb_name": arrayKey[6],
      "member_fb_email": arrayKey[7],
      "member_fb_phone": arrayKey[8],
      "member_fb_photo": arrayKey[9],
      "member_location_url": arrayKey[10],
      "member_location_full": arrayKey[11],
    });
  }
}

class UninitializedAuthenticationRegister extends AuthenticationRegister {}

//===========================================================================
//CHEKC DATA MEMBER ADA ATAU TIDAK | PAGE LOGIN INPUT NOMOR HP
class AuthenticationCheck {
  String status;
  AuthenticationCheck({this.status});

  factory AuthenticationCheck.checkAuth(Map<String, dynamic> object) {
    return AuthenticationCheck(status: object['status']);
  }

  static Future<AuthenticationCheck> postCheckAuthFromAPI(String key) async {
    String apiURL = Config().ipmain + Config().authcheck;

    var apiResult = await http.post(apiURL, body: {"member_phone": key});
    var jsonObject = json.decode(apiResult.body);

    return AuthenticationCheck.checkAuth(jsonObject);
  }
}

class UninitializedAuthenticationCheck extends AuthenticationCheck {}
