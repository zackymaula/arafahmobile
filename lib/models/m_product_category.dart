import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class ProductListCategory {
  String idproducts;
  String idcategories;
  String categoryname;
  String idcategorysub;
  String categorysubname;
  String productname;
  String productpricehk;
  String productpricetw;
  String productpricesg;
  String productpricemy;
  String productfilepath;
  String productfilename;
  String description;

  ProductListCategory(
      {this.idproducts,
      this.idcategories,
      this.categoryname,
      this.idcategorysub,
      this.categorysubname,
      this.productname,
      this.productpricehk,
      this.productpricetw,
      this.productpricesg,
      this.productpricemy,
      this.productfilepath,
      this.productfilename,
      this.description});

  factory ProductListCategory.createProductListCategory(Map<String, dynamic> object) {
    return ProductListCategory(
      idproducts: object['id_products'].toString(),
      idcategories: object['id_categories'].toString(),
      categoryname: object['category_name'],
      idcategorysub: object['id_category_sub'].toString(),
      categorysubname: object['categorysub_name'],
      productname: object['product_name'],
      productpricehk: (object['product_price_hk'] == null || object['product_price_hk'] == '') ? '-' : object['product_price_hk'],
      productpricetw: (object['product_price_tw'] == null || object['product_price_tw'] == '') ? '-' : object['product_price_tw'],
      productpricesg: (object['product_price_sg'] == null || object['product_price_sg'] == '') ? '-' : object['product_price_sg'],
      productpricemy: (object['product_price_my'] == null || object['product_price_my'] == '') ? '-' : object['product_price_my'],
      productfilepath: object['product_file_path'],
      productfilename: object['product_file_name'],
      description: object['description'],
    );
  }

  static Future<List<ProductListCategory>> getProductListCategoryFromAPI(int idcategories) async {
    String apiURL = Config().ipmain + Config().getproductlistcategory + idcategories.toString();

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> productListCategoryData = (jsonObject as Map<String, dynamic>)['data'];

    List<ProductListCategory> productListCategoryArray = [];
    for (var i = 0; i < productListCategoryData.length; i++)
      productListCategoryArray.add(ProductListCategory.createProductListCategory(productListCategoryData[i]));

    return productListCategoryArray;
  }
}

class UninitializedProductListCategory extends ProductListCategory {}
