import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class CategoryList {
  int idcategories;
  String name;
  String filepath;
  String filename;

  CategoryList(
      {this.idcategories,
      this.name,
      this.filepath,
      this.filename});

  factory CategoryList.createCategoryList(Map<String, dynamic> object) {
    return CategoryList(
      idcategories: object['id_categories'],
      name: object['name'],
      filepath: object['file_path'],
      filename: object['file_name'],
    );
  }

  static Future<List<CategoryList>> getCategoryListFromAPI() async {
    String apiURL = Config().ipmain + Config().getcategorylist;

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> categoryListData = (jsonObject as Map<String, dynamic>)['data'];

    List<CategoryList> categoryListArray = [];
    for (var i = 0; i < categoryListData.length; i++)
      categoryListArray.add(CategoryList.createCategoryList(categoryListData[i]));

    return categoryListArray;
  }
}

class UninitializedCategoryList extends CategoryList {}
