import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

//BLOC CART LIST
class CartList {
  String idcart;
  String idmember;
  String idproducts;
  String idcategories;
  String categoryname;
  String idcategorysub;
  String categorysubname;
  String productname;
  String productpricehk;
  String productpricetw;
  String productpricesg;
  String productpricemy;
  String productfilepath;
  String productfilename;
  String description;

  CartList(
      {this.idcart,
      this.idmember,
      this.idproducts,
      this.idcategories,
      this.categoryname,
      this.idcategorysub,
      this.categorysubname,
      this.productname,
      this.productpricehk,
      this.productpricetw,
      this.productpricesg,
      this.productpricemy,
      this.productfilepath,
      this.productfilename,
      this.description});

  factory CartList.createCartList(Map<String, dynamic> object) {
    return CartList(
      idcart: object['id_cart'].toString(),
      idmember: object['id_member'].toString(),
      idproducts: object['id_products'].toString(),
      idcategories: object['id_categories'].toString(),
      categoryname: object['category_name'],
      idcategorysub: object['id_category_sub'].toString(),
      categorysubname: object['categorysub_name'],
      productname: object['product_name'],
      productpricehk: (object['product_price_hk'] == null ||
              object['product_price_hk'] == '')
          ? '-'
          : object['product_price_hk'],
      productpricetw: (object['product_price_tw'] == null ||
              object['product_price_tw'] == '')
          ? '-'
          : object['product_price_tw'],
      productpricesg: (object['product_price_sg'] == null ||
              object['product_price_sg'] == '')
          ? '-'
          : object['product_price_sg'],
      productpricemy: (object['product_price_my'] == null ||
              object['product_price_my'] == '')
          ? '-'
          : object['product_price_my'],
      productfilepath: object['product_file_path'],
      productfilename: object['product_file_name'],
      description: object['description'],
    );
  }

  static Future<List<CartList>> getCartListFromAPI(String generatekey) async {
    String apiURL = Config().ipmain + Config().cartlist;

    var arrayGenerateKey = generatekey.split("+space+");
    var apiResult = await http.post(apiURL, body: {
      "member_generate_id": arrayGenerateKey[0],
      "member_phone": arrayGenerateKey[1],
    });
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> cartListData = (jsonObject as Map<String, dynamic>)['data'];

    List<CartList> cartListArray = [];
    for (var i = 0; i < cartListData.length; i++)
      cartListArray.add(CartList.createCartList(cartListData[i]));

    return cartListArray;
  }
}

class UninitializedCartList extends CartList {}

//BLOC CART INSERT | CLICK ADD CART DI PRODUCT DETAIL
class CartInsert {
  static Future<CartInsert> postCartInsertAPI(String generatekey) async {
    String apiURL = Config().ipmain + Config().cartinsert;

    var arrayGenerateKey = generatekey.split("+space+");
    await http.post(apiURL, body: {
      "member_generate_id": arrayGenerateKey[0],
      "member_phone": arrayGenerateKey[1],
      "id_product": arrayGenerateKey[2],
      "id_promo": arrayGenerateKey[3],
    });
  }
}

class UninitializedCartInsert extends CartInsert {}

//BLOC CART DELETE | CLICK DELETE CART
class CartDelete {
  static Future<CartDelete> sendCartDeleteAPI(String id_cart) async {
    String apiURL = Config().ipmain + Config().cartdelete + id_cart;

    await http.get(apiURL);
  }
}

class UninitializedCartDelete extends CartDelete {}
