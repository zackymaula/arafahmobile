import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class MGCatalogCategorySub {
  String id;
  String name;
  String sku;
  String price;
  String description;
  String image;
  String categorynames;
  String categorysubnames;

  MGCatalogCategorySub(
      {this.id,
      this.name,
      this.sku,
      this.price,
      this.description,
      this.image,
      this.categorynames,
      this.categorysubnames});

  factory MGCatalogCategorySub.createCatalogCategorySub(
      Map<String, dynamic> object) {
    return MGCatalogCategorySub(
      id: object['id'],
      name: object['name'],
      sku: object['sku'],
      price: object['price'],
      description: object['description'],
      image: object['image'],
      categorynames: object['categorynames'],
      categorysubnames: object['categorysubnames'],
    );
  }

  static Future<List<MGCatalogCategorySub>> getCatalogCategorySubFromApiMG(
      String data) async {
    String apiURL = Config().ipmain + Config().mggetcatalogcategorysub;

    var arrayData = data.split("+space+");
    var apiResult = await http.post(apiURL, body: {
      "post_category": arrayData[0],
      "post_categorysub": arrayData[1],
    });
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> catalogCategorySubData =
        (jsonObject as Map<String, dynamic>)['data'];

    List<MGCatalogCategorySub> catalogCategorySubArray = [];
    for (var i = 0; i < catalogCategorySubData.length; i++)
      catalogCategorySubArray.add(MGCatalogCategorySub.createCatalogCategorySub(
          catalogCategorySubData[i]));

    return catalogCategorySubArray;
  }
}

class UninitializedMGCatalogCategorySub extends MGCatalogCategorySub {}
