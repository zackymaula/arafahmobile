import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class PromoList {
  String idpromo;
  String promoname;
  String promopricehk;
  String promopricetw;
  String promopricesg;
  String promopricemy;
  String promofilepath;
  String promofilename;
  String promodescription;

  PromoList(
      {this.idpromo,
      this.promoname,
      this.promopricehk,
      this.promopricetw,
      this.promopricesg,
      this.promopricemy,
      this.promofilepath,
      this.promofilename,
      this.promodescription});

  factory PromoList.createPromoList(Map<String, dynamic> object) {
    return PromoList(
      idpromo: object['id_promo'].toString(),
      promoname: object['promo_name'],
      promopricehk: (object['promo_price_hk'] == null || object['promo_price_hk'] == '') ? '-' : object['promo_price_hk'],
      promopricetw: (object['promo_price_tw'] == null || object['promo_price_tw'] == '') ? '-' : object['promo_price_tw'],
      promopricesg: (object['promo_price_sg'] == null || object['promo_price_sg'] == '') ? '-' : object['promo_price_sg'],
      promopricemy: (object['promo_price_my'] == null || object['promo_price_my'] == '') ? '-' : object['promo_price_my'],
      promofilepath: object['promo_file_path'],
      promofilename: object['promo_file_name'],
      promodescription: object['promo_description'],
    );
  }

  static Future<List<PromoList>> getPromoListFromAPI() async {
    String apiURL = Config().ipmain + Config().getpromolist;

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> promoListData = (jsonObject as Map<String, dynamic>)['data'];

    List<PromoList> promoListArray = [];
    for (var i = 0; i < promoListData.length; i++)
      promoListArray.add(PromoList.createPromoList(promoListData[i]));

    return promoListArray;
  }
}

class UninitializedPromoList extends PromoList {}
