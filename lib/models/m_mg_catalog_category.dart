import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class MGCatalogCategory {
  String id;
  String name;
  String sku;
  String price;
  String description;
  String image;
  String categorynames;
  String categorysubnames;

  MGCatalogCategory(
      {this.id,
      this.name,
      this.sku,
      this.price,
      this.description,
      this.image,
      this.categorynames,
      this.categorysubnames});

  factory MGCatalogCategory.createCatalogCategory(Map<String, dynamic> object) {
    return MGCatalogCategory(
      id: object['id'],
      name: object['name'],
      sku: object['sku'],
      price: object['price'],
      description: object['description'],
      image: object['image'],
      categorynames: object['categorynames'],
      categorysubnames: object['categorysubnames'],
    );
  }

  static Future<List<MGCatalogCategory>> getCatalogCategoryFromApiMG(
      String data) async {
    String apiURL = Config().ipmain + Config().mggetcatalogcategory;

    var apiResult = await http.post(apiURL, body: {
      "post_category": data,
    });
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> catalogCategoryData =
        (jsonObject as Map<String, dynamic>)['data'];

    List<MGCatalogCategory> catalogCategoryArray = [];
    for (var i = 0; i < catalogCategoryData.length; i++)
      catalogCategoryArray
          .add(MGCatalogCategory.createCatalogCategory(catalogCategoryData[i]));

    return catalogCategoryArray;
  }
}

class UninitializedMGCatalogCategory extends MGCatalogCategory {}
