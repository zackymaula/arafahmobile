import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class CategorySubList {
  int idcategories;
  int idcategorysub;
  String categoryname;
  String categorysubname;
  String filepath;
  String filename;

  CategorySubList(
      {
      this.idcategories,
      this.idcategorysub,
      this.categoryname,
      this.categorysubname,
      this.filepath,
      this.filename});

  factory CategorySubList.createCategorySubList(Map<String, dynamic> object) {
    return CategorySubList(
      idcategories: object['id_categories'],
      idcategorysub: object['id_category_sub'],
      categoryname: object['category_name'],
      categorysubname: object['categorysub_name'],
      filepath: object['file_path'],
      filename: object['file_name'],
    );
  }

  static Future<List<CategorySubList>> getCategorySubListFromAPI(int idcategories) async {
    String apiURL = Config().ipmain + Config().getcategorysublist + idcategories.toString();

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> categorySubListData = (jsonObject as Map<String, dynamic>)['data'];

    List<CategorySubList> categorySubListArray = [];
    for (var i = 0; i < categorySubListData.length; i++)
      categorySubListArray.add(CategorySubList.createCategorySubList(categorySubListData[i]));

    return categorySubListArray;
  }
}

class UninitializedCategorySubList extends CategorySubList {}
