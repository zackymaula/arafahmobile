import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class MGCatalogAll {
  String id;
  String name;
  String sku;
  String price;
  String description;
  String image;
  String categorynames;
  String categorysubnames;

  MGCatalogAll(
      {this.id,
      this.name,
      this.sku,
      this.price,
      this.description,
      this.image,
      this.categorynames,
      this.categorysubnames});

  factory MGCatalogAll.createCatalogAll(Map<String, dynamic> object) {
    return MGCatalogAll(
      id: object['id'],
      name: object['name'],
      sku: object['sku'],
      price: object['price'],
      description: object['description'],
      image: object['image'],
      categorynames: object['categorynames'],
      categorysubnames: object['categorysubnames'],
    );
  }

  static Future<List<MGCatalogAll>> getCatalogAllFromApiMG() async {
    String apiURL = Config().ipmain + Config().mggetcatalogall;

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> catalogAllData = (jsonObject as Map<String, dynamic>)['data'];

    List<MGCatalogAll> catalogAllArray = [];
    for (var i = 0; i < catalogAllData.length; i++)
      catalogAllArray.add(MGCatalogAll.createCatalogAll(catalogAllData[i]));

    return catalogAllArray;
  }
}

class UninitializedMGCatalogAll extends MGCatalogAll {}
