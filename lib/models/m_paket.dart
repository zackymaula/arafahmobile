import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class PaketList {
  String idpaket;
  String filepath;
  String filename;

  PaketList(
      {this.idpaket,
      this.filepath,
      this.filename});

  factory PaketList.createPaketList(Map<String, dynamic> object) {
    return PaketList(
      idpaket: object['id_paket'].toString(),
      filepath: object['file_path'],
      filename: object['file_name'],
    );
  }

  static Future<List<PaketList>> getPaketListFromAPI() async {
    String apiURL = Config().ipmain + Config().getpaketlist;

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> paketListData = (jsonObject as Map<String, dynamic>)['data'];

    List<PaketList> paketListArray = [];
    for (var i = 0; i < paketListData.length; i++)
      paketListArray.add(PaketList.createPaketList(paketListData[i]));

    return paketListArray;
  }
}

class UninitializedPaketList extends PaketList {}
