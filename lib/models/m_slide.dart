import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

class SlideList {
  String idslide;
  String filepath;
  String filename;

  SlideList({this.idslide, this.filepath, this.filename});

  factory SlideList.createSlideList(Map<String, dynamic> object){
    return SlideList(
      idslide: object['id_slide'].toString(),
      filepath: object['file_path'],
      filename: object['file_name'],
    );
  }

  static Future<List<SlideList>> getSlideListFromAPI() async {
    String apiURL = Config().ipmain + Config().getslidelist;

    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> slideListData = (jsonObject as Map<String, dynamic>)['data'];

    List<SlideList> slideListArray = [];
    for (var i = 0; i < slideListData.length; i++) 
      slideListArray.add(SlideList.createSlideList(slideListData[i]));

    return slideListArray;
  }
}

class UninitializedSlideList extends SlideList{}