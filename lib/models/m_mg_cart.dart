import 'dart:convert';
import 'package:arafahmobile/configs/config.dart';
import 'package:http/http.dart' as http;

//VIEW LIST CART
class MGCartList {
  String idcart;
  String id;
  String name;
  String sku;
  String price;
  String description;
  String image;
  String categorynames;
  String categorysubnames;

  MGCartList(
      {this.idcart,
      this.id,
      this.name,
      this.sku,
      this.price,
      this.description,
      this.image,
      this.categorynames,
      this.categorysubnames});

  factory MGCartList.createCartList(Map<String, dynamic> object) {
    return MGCartList(
      idcart: object['id_cart'],
      id: object['catalog_id'],
      name: object['catalog_name'],
      sku: object['catalog_sku'],
      price: object['catalog_price'],
      description: object['catalog_description'],
      image: object['catalog_image'],
      categorynames: object['catalog_categorynames'],
      categorysubnames: object['catalog_categorysubnames'],
    );
  }

  static Future<List<MGCartList>> getCartListFromApi(String data) async {
    String apiURL = Config().ipmain + Config().mgcartlist;

    var arrayData = data.split("+space+");
    var apiResult = await http.post(apiURL, body: {
      "member_generate_id": arrayData[0],
      "member_phone": arrayData[1],
    });
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> cartListData = (jsonObject as Map<String, dynamic>)['data'];

    List<MGCartList> carListArray = [];
    for (var i = 0; i < cartListData.length; i++)
      carListArray.add(MGCartList.createCartList(cartListData[i]));

    return carListArray;
  }
}

class UninitializedMGCartList extends MGCartList {}

//BLOC CART INSERT | CLICK ADD CART DI PRODUCT DETAIL | cart_mg
class MGCartInsert {
  static Future<MGCartInsert> postMGCartInsertAPI(String data) async {
    String apiURL = Config().ipmain + Config().mgcartinsert;

    var arrayData = data.split("+space+");
    await http.post(apiURL, body: {
      "member_generate_id": arrayData[0],
      "member_phone": arrayData[1],
      "post_id": arrayData[2],
      "post_name": arrayData[3],
      "post_sku": arrayData[4],
      "post_price": arrayData[5],
      "post_description": arrayData[6],
      "post_image":
          "https://www.arafah.hk/pub/media/catalog/product" + arrayData[7],
      "post_categorynames": arrayData[8],
      "post_categorysubnames": arrayData[9],
    });
  }
}

class UninitializedMGCartInsert extends MGCartInsert {}

//BLOC CART DELETE | CLICK DELETE CART
class MGCartDelete {
  static Future<MGCartDelete> sendCartDeleteAPI(String data) async {
    String apiURL = Config().ipmain + Config().mgcartdelete + data;

    await http.get(apiURL);
  }
}

class UninitializedMGCartDelete extends MGCartDelete {}
