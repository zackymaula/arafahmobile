import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/login/page_input_nomor.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

void main() {
  //DISABLE ORIESTASI
  //SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
    MaterialApp(
      //theme: ThemeData(primarySwatch: Colors.grey),
      debugShowCheckedModeBanner: false, //DISABLE BANNER DEBUG
      home: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;
  //var _currency = false;
  var _statusLogin = false;

  //GET CURRENCY
  // _getPrefCurrency() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   return pref.getString(Config().prefcurrency) ?? "";
  // }

  //CHECK KEY AUTH SHARE PREF > JIKA KOSONG LOGIN SET FALSE
  _getPrefKeyLog() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefmemberkeylog) ?? "";
  }

  //DECRYPT KEY > JIKA GAGAL STATUS LOGIN SET FALSE
  _decryptKey(String keylog) {
    try {
      final key = encrypt.Key.fromUtf8(Config().key);
      final iv = encrypt.IV.fromLength(16);
      final encrypter = encrypt.Encrypter(encrypt.AES(key));

      final convertkeylog = encrypt.Encrypted.fromBase64(keylog);
      encrypter.decrypt(convertkeylog, iv: iv);
      _statusLogin = true;

      setState(() {});
    } catch (e) {
      //JIKA DECRYPT GAGAL > LOGIN SET FALSE
      _statusLogin = false;
      setState(() {});
    }
  }

  @override
  void initState() {
    // _getPrefCurrency().then((i) {
    //   (i == '') ? _currency = false : _currency = true;
    //   setState(() {});
    // });
    _getPrefKeyLog().then((k) {
      (k == '') ? _statusLogin = false : _decryptKey(k);
      setState(() {});
    });
    _getCurrentLocation();

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  _page() {
    var _return;

    // if (_currency == false) {
    //   _return = PageCurrency();
    // } else {
    //   _return = PageMain(loginStatus: _statusLogin);
    // }

    if (_statusLogin == false) {
      _return = PageLoginInputNomor();
    } else {
      _return = PageMain();
    }

    return _return;
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 6,
      navigateAfterSeconds: _page(),
      // title: Text(
      //   "Selamat Datang",
      //   style: TextStyle(
      //     fontFamily: "MadeTommySoft",
      //     fontSize: 30,
      //     color: Colors.white,
      //   ),
      // ),
      image:
          Image.asset("assets/images/icons/logo-arafahelectronics-white.png"),
      backgroundColor: Colors.blue[900],
      photoSize: 100,
      loaderColor: Colors.white,
    );
  }

  //GET LOCATION LAT LONG
  _getCurrentLocation() async {
    //final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    try {
      await geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) {
        setState(() {
          _currentPosition = position;
        });

        _getAddressFromLatLng();
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }
  }

  //GET LOCATION FULL
  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.thoroughfare}, ${place.subThoroughfare}, ${place.subLocality}, ${place.locality}, ${place.subAdministrativeArea}, ${place.administrativeArea}, ${place.country}, ${place.postalCode}";

        //print(_currentPosition);
        //print(_currentAddress);
        _savePrefData();
      });
    } catch (e) {
      print(e);
    }
  }

  //SET LOCATION TO PREF
  _savePrefData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberlocationurl,
          'https://www.google.co.id/maps/@${_currentPosition.latitude.toString()},${_currentPosition.longitude.toString()},20z');
      pref.setString(
          Config().prefmemberlocationfull, _currentAddress.toString());
    });
  }
}
