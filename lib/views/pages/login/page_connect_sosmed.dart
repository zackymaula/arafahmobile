import 'dart:convert';
import 'dart:typed_data';

import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/login/page_input_otp.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageLoginConnectSosmed extends StatefulWidget {
  @override
  _PageLoginConnectSosmedState createState() => _PageLoginConnectSosmedState();
}

class _PageLoginConnectSosmedState extends State<PageLoginConnectSosmed> {
  Color statusColor;
  var statusInfo;
  var statusIcon;
  bool statusKoneksi = false;
  var detailInfoFacebookName = '';
  var detailInfoFacebookEmail = '';
  var detailInfoFacebookPhone = '';
  var detailInfoFacebookPhoto = '';
  //var actionButtonKoneksi;

  final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>(); //UNTUK SNACKBAR

  FacebookLogin fbLogin = FacebookLogin();

  @override
  void initState() {
    kondisiAwal();
    super.initState();
  }

  kondisiAwal() {
    if (statusKoneksi == false) {
      setState(() {
        statusInfo = 'FACEBOOK BELUM TERKONEKSI';
        statusColor = Colors.red;
        statusIcon = Icons.cancel;
        //return () => actionButtonKoneksi();
      });
    } else {
      setState(() {
        statusInfo = 'FACEBOOK TERKONEKSI';
        statusColor = Colors.green;
        statusIcon = Icons.verified_user;
        //return null;
      });
    }
  }

  actionButtonKoneksi() async {
    await fbLogin.logIn(['email', 'public_profile']).then((result) {
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          FirebaseAuth.instance
              //.signInWithCustomToken(token: result.accessToken.token)
              .signInWithCredential(FacebookAuthProvider.getCredential(
                  accessToken: result.accessToken.token))
              .then((signInUser) {
            //print('Signed in as ${signInUser.user.displayName}');
            // Navigator.of(context).push(
            //   MaterialPageRoute(
            //     builder: (BuildContext context) =>
            //         PageLoginInputOTP(phoneNo: signInUser.user.displayName),
            //   ),
            // );
            setState(() {
              statusKoneksi = true;
              detailInfoFacebookName = signInUser.user.displayName.toString();
              detailInfoFacebookEmail = signInUser.user.email.toString();
              detailInfoFacebookPhone = signInUser.user.phoneNumber.toString();
              detailInfoFacebookPhoto = signInUser.user.photoUrl.toString();
              kondisiAwal();
            });
          }).catchError((e) {
            print(e);
            setState(() {
              statusKoneksi = false;
              kondisiAwal();
            });
          });
          break;
        case FacebookLoginStatus.cancelledByUser:
          // TODO: Handle this case.
          setState(() {
            statusKoneksi = false;
            kondisiAwal();
          });
          break;
        case FacebookLoginStatus.error:
          // TODO: Handle this case.
          setState(() {
            statusKoneksi = false;
            kondisiAwal();
          });
          break;
      }
    }).catchError((e) {
      setState(() {
        statusKoneksi = false;
        kondisiAwal();
      });
    });
  }

  prosesButtonLanjutkan(context) {
    if (statusKoneksi == false) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Mohon koneksikan dengan FACEBOOK',
            style: TextStyle(
              fontFamily: "MadeTommySoft",
            ),
          ),
          backgroundColor: Colors.redAccent,
          duration: Duration(seconds: 1),
        ),
      );
    } else {
      savePrefData();
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageLoginInputOTP(),
        ),
      );
    }
  }

  //SET SHARED PREF
  void savePrefData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberfacebookname, detailInfoFacebookName);
      pref.setString(Config().prefmemberfacebookemail, detailInfoFacebookEmail);
      pref.setString(Config().prefmemberfacebookphone, detailInfoFacebookPhone);
      pref.setString(Config().prefmemberfacebookphoto, detailInfoFacebookPhoto);
    });
  }

  // Uint8List dataprefphotoidcarddecoded;
  // Future<Null> _getPref() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   var dataprefphotoidcard =
  //       pref.getString(Config().prefmemberphotoidcard) ?? "";
  //   await decodebase64(dataprefphotoidcard);
  // }

  // decodebase64(String dataprefphotoidcard) {
  //   dataprefphotoidcarddecoded = base64Decode(dataprefphotoidcard);
  // }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            //BACKGROUND
            // Container(
            //   height: MediaQuery.of(context).size.height / 1,
            //   decoration: BoxDecoration(
            //     // gradient: LinearGradient(
            //     //   colors: [Colors.blue[100], Colors.blue[800]],
            //     //   begin: FractionalOffset.topCenter,
            //     // ),
            //     color: Colors.blue[800],
            //   ),
            // ),

            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  // Container(
                  //   width: MediaQuery.of(context).size.width / 2,
                  //   height: MediaQuery.of(context).size.height / 5,
                  //   //color: Colors.lightBlue,
                  //   child: Image.memory(dataprefphotoidcarddecoded),
                  // ),
                  //LOGO
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 5,
                    //color: Colors.lightBlue,
                    child: Image.asset(
                        "assets/images/icons/logo-arafahelectronics.png"),
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width / 5,
                    height: MediaQuery.of(context).size.height / 5,
                    //color: Colors.lightBlue,
                    child: Image.asset("assets/images/icons/facebook-icon.png"),
                  ),

                  Container(
                    child: Icon(
                      statusIcon,
                      color: statusColor,
                      size: 70,
                    ),
                  ),

                  SizedBox(height: 10),
                  Text(
                    statusInfo,
                    style: TextStyle(
                      color: statusColor,
                      fontSize: 20,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),

                  SizedBox(height: 30),
                  // Text(
                  //   detailInfoFacebookName +
                  //       ' - ' +
                  //       detailInfoFacebookEmail +
                  //       ' - ' +
                  //       detailInfoFacebookPhone +
                  //       ' - ' +
                  //       detailInfoFacebookPhoto,
                  //   style: TextStyle(
                  //     color: Colors.blue[900],
                  //     fontSize: 20,
                  //     fontWeight: FontWeight.bold,
                  //   ),
                  // ),

                  (statusKoneksi == false)
                      ? Container()
                      : Card(
                          margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
                          elevation: 5,
                          shape: Border(
                            bottom:
                                BorderSide(color: Colors.blue[800], width: 10),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(40),
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                              'assets/images/icons/facebook-icon.png'),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 20),
                                    Icon(
                                      Icons.compare_arrows,
                                      color: Colors.green,
                                      size: 30,
                                    ),
                                    SizedBox(width: 20),
                                    CachedNetworkImage(
                                      imageUrl: detailInfoFacebookPhoto,
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                        height: 60,
                                        width: 60,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40),
                                          image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      placeholder: (context, url) => Center(
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2,
                                          backgroundColor: Colors.orangeAccent,
                                        ),
                                      ),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    ),
                                  ],
                                ),
                                // CachedNetworkImage(
                                //   imageUrl: detailInfoFacebookPhoto,
                                //   imageBuilder: (context, imageProvider) =>
                                //       Container(
                                //     height: 70,
                                //     width: 70,
                                //     decoration: BoxDecoration(
                                //       borderRadius: BorderRadius.circular(40),
                                //       image: DecorationImage(
                                //         image: imageProvider,
                                //         fit: BoxFit.cover,
                                //       ),
                                //     ),
                                //   ),
                                //   placeholder: (context, url) => Center(
                                //     child: CircularProgressIndicator(),
                                //   ),
                                //   errorWidget: (context, url, error) =>
                                //       Icon(Icons.error),
                                // ),
                                SizedBox(height: 15),
                                Text(
                                  detailInfoFacebookName,
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                    fontSize: 25,
                                    fontFamily: "MadeTommySoft",
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  detailInfoFacebookEmail,
                                  style: TextStyle(
                                    color: Colors.blue[900],
                                    fontSize: 15,
                                    fontFamily: "MadeTommySoft",
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                  SizedBox(height: 30),
                  //TOMBOL KONEKSIKAN DENGAN FACEBOOK
                  (statusKoneksi == true)
                      ? Container()
                      : Container(
                          width: MediaQuery.of(context).size.width / 1,
                          height: 70,
                          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
                          padding: EdgeInsets.all(10),
                          child: RaisedButton(
                            color: Colors.blue[900],
                            child: Text(
                              'Koneksikan dengan FACEBOOK',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(40),
                              ),
                            ),
                            //onPressed: (statusKoneksi == true) ? null : () {},
                            onPressed: () => actionButtonKoneksi(),
                          ),
                        ),

                  SizedBox(height: 100),
                ],
              ),
            ),

            //BUTTON NEXT
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(),
                  Row(
                    children: <Widget>[
                      // Flexible(
                      //   flex: 1,
                      //   child: Container(
                      //     height: 70,
                      //     padding: EdgeInsets.all(10),
                      //     child: RaisedButton(
                      //       color: Colors.orangeAccent,
                      //       child: Icon(
                      //         Icons.keyboard_arrow_left,
                      //         color: Colors.white,
                      //       ),
                      //       shape: RoundedRectangleBorder(
                      //         borderRadius: BorderRadius.all(
                      //           Radius.circular(30),
                      //         ),
                      //       ),
                      //       onPressed: () {
                      //         Navigator.of(context).pop();
                      //       },
                      //     ),
                      //   ),
                      // ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: RaisedButton(
                            color: Colors.orangeAccent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Lanjutkan",
                                  style: TextStyle(
                                    fontFamily: "MadeTommySoft",
                                    fontSize: 23,
                                    color: Colors.white,
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_right,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                            ),
                            onPressed: () => prosesButtonLanjutkan(context),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
