import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/login/page_connect_sosmed.dart';
import 'package:arafahmobile/views/pages/login/page_photo_id_card.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageLoginInputDetail extends StatefulWidget {
  @override
  _PageLoginInputDetailState createState() => _PageLoginInputDetailState();
}

class _PageLoginInputDetailState extends State<PageLoginInputDetail> {
  var textFieldNamaDepan = TextEditingController();
  var textFieldNamaBelakang = TextEditingController();
  var textFieldTanggalLahir = '';

  final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>(); //UNTUK SNACKBAR

  //DATE PICKER
  DateTime selectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked; //2020/02/12 00:00:00.000
        textFieldTanggalLahir =
            "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}";
        //selectedDate.toString().split(' ')[0].replaceAll(RegExp('-'), '/');
        //"${selectedDate.toLocal()}".split(' ')[0]
      });
  }

  prosesButtonLanjutkan() {
    if (textFieldNamaDepan.text == '' || textFieldTanggalLahir == '') {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Mohon melengkapi data',
            style: TextStyle(
              fontFamily: "MadeTommySoft",
            ),
          ),
          backgroundColor: Colors.redAccent,
          duration: Duration(seconds: 1),
        ),
      );
    } else {
      savePrefData();
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageLoginConnectSosmed(),
        ),
      );
    }
  }

  //SET CURRENCY
  void savePrefData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmembernamadepan, textFieldNamaDepan.text);
      pref.setString(
          Config().prefmembernamabelakang, textFieldNamaBelakang.text);
      pref.setString(Config().prefmembertgllahir, textFieldTanggalLahir);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context)
          .requestFocus(FocusNode()), //untuk menutup keyboard,
      child: Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            //BACKGROUND
            Container(
              height: MediaQuery.of(context).size.height / 1,
              decoration: BoxDecoration(
                // gradient: LinearGradient(
                //   colors: [Colors.blue[100], Colors.blue[800]],
                //   begin: FractionalOffset.topCenter,
                // ),
                color: Colors.blue[800],
              ),
            ),

            //CONTENT
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //LOGO
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 3,
                    //color: Colors.lightBlue,
                    child: Image.asset(
                        "assets/images/icons/logo-arafahelectronics-white.png"),
                  ),

                  //INPUT NAMA DEPAN
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    padding: EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: TextFormField(
                      controller: textFieldNamaDepan,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          //filled: true,
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.black,
                          ),
                          labelText: "Masukkan Nama Depan",
                          hintText: "Nama Depan",
                          hintStyle: TextStyle(
                            fontSize: 20,
                            fontFamily: "MadeTommySoft",
                          ),
                          border: InputBorder.none
                          // border: OutlineInputBorder(
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          ),
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                  ),

                  SizedBox(height: 30),

                  //INPUT NAMA BELAKANG
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    padding: EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: TextFormField(
                      controller: textFieldNamaBelakang,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          //filled: true,
                          prefixIcon: Icon(
                            Icons.person_outline,
                            color: Colors.black,
                          ),
                          labelText: "Masukkan Nama Belakang",
                          hintText: "Nama Belakang",
                          hintStyle: TextStyle(
                            fontSize: 20,
                            fontFamily: "MadeTommySoft",
                          ),
                          border: InputBorder.none
                          // border: OutlineInputBorder(
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          ),
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                  ),

                  SizedBox(height: 30),

                  //INPUT TANGGAL LAHIR
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      padding: EdgeInsets.all(0),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextFormField(
                        enabled: false,
                        //controller: textFieldTanggalLahir,
                        controller: TextEditingController(
                            //text: "${selectedDate.toLocal()}".split(' ')[0])
                            text: textFieldTanggalLahir),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            //filled: true,
                            prefixIcon: Icon(
                              Icons.date_range,
                              color: Colors.black,
                            ),
                            labelText: "Masukkan Tanggal Lahir",
                            hintText: "Nama Belakang",
                            hintStyle: TextStyle(
                              fontSize: 20,
                              fontFamily: "MadeTommySoft",
                            ),
                            border: InputBorder.none
                            // border: OutlineInputBorder(
                            //   borderRadius: BorderRadius.circular(10),
                            // ),
                            ),
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: "MadeTommySoft",
                        ),
                      ),
                    ),
                    onTap: () => _selectDate(context),
                  ),

                  SizedBox(height: 100),
                ],
              ),
            ),

            //BUTTON NEXT
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(),
                  Row(
                    children: <Widget>[
                      // Flexible(
                      //   flex: 1,
                      //   child: Container(
                      //     height: 70,
                      //     padding: EdgeInsets.all(10),
                      //     child: RaisedButton(
                      //       color: Colors.orangeAccent,
                      //       child: Icon(
                      //         Icons.keyboard_arrow_left,
                      //         color: Colors.white,
                      //       ),
                      //       shape: RoundedRectangleBorder(
                      //         borderRadius: BorderRadius.all(
                      //           Radius.circular(30),
                      //         ),
                      //       ),
                      //       onPressed: () {
                      //         Navigator.of(context).pop();
                      //       },
                      //     ),
                      //   ),
                      // ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: RaisedButton(
                            color: Colors.orangeAccent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Lanjutkan",
                                  style: TextStyle(
                                    fontSize: 23,
                                    color: Colors.white,
                                    fontFamily: "MadeTommySoft",
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_right,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                            ),
                            onPressed: () => prosesButtonLanjutkan(),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
