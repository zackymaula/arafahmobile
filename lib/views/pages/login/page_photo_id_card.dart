import 'dart:io';
import 'dart:convert';

import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/login/page_connect_sosmed.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageLoginPhotoIdCard extends StatefulWidget {
  @override
  _PageLoginPhotoIdCardState createState() => _PageLoginPhotoIdCardState();
}

class _PageLoginPhotoIdCardState extends State<PageLoginPhotoIdCard> {
  CameraController cameraController;
  List cameras;
  int selectedCameraIndex;
  String imgPath = '';
  String base64Image;

  final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>(); //UNTUK SNACKBAR

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (cameraController != null) {
      await cameraController.dispose();
    }
    cameraController =
        CameraController(cameraDescription, ResolutionPreset.high);
    cameraController.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });
    if (cameraController.value.hasError) {
      print('Camera error ${cameraController.value.errorDescription}');
    }
    try {
      await cameraController.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }
    if (mounted) {
      setState(() {});
    }
  }

  //Display camera preview
  Widget _cameraPreviewWidget() {
    if (cameraController == null || !cameraController.value.isInitialized) {
      return const Text(
        'loading...',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
      );
    }
    return AspectRatio(
      aspectRatio: cameraController.value.aspectRatio,
      child: CameraPreview(cameraController),
    );
  }

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        _initCameraController(cameras[selectedCameraIndex]).then((void v) {});
      } else {
        print('No camera available');
      }
    }).catchError((err) {
      print('Error : ${err.code}');
    });
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error : ${e.code}\n Error message : ${e.description}';
    print(errorText);
  }

  //KETIKA TOMBOL PHOTO DITEKAN
  void _onCapturePressed(BuildContext context) async {
    try {
      final path =
          join((await getTemporaryDirectory()).path, '${DateTime.now()}.png');
      await cameraController.takePicture(path);
      setState(() {
        imgPath = path;
        convertImageToBase64(path);
      });
    } catch (e) {
      _showCameraException(e);
    }
  }

  //CONVERT IMAGE TO BASE64
  convertImageToBase64(String imagePath) {
    File imageFile = new File(imagePath);
    List<int> imageBytes = imageFile.readAsBytesSync();
    base64Image = base64Encode(imageBytes); //base64Encode(imageBytes);
  }

  prosesButtonLanjutkan(context) {
    if (imgPath == '') {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Mohon foto KTP Luar Negeri',
            style: TextStyle(
              fontFamily: "MadeTommySoft",
            ),
          ),
          backgroundColor: Colors.redAccent,
          duration: Duration(seconds: 1),
        ),
      );
    } else {
      savePrefData();
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageLoginConnectSosmed(),
        ),
      );
    }
  }

  //SET SHARED PREF
  void savePrefData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberphotoidcard, base64Image);
    });
  }

  Widget widgetTombolFoto(context) {
    if (imgPath == '') {
      return Container(
        height: 70,
        padding: EdgeInsets.all(10),
        child: RaisedButton(
          color: Colors.white,
          child: Icon(
            Icons.photo_camera,
            color: Colors.black,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(40),
            ),
          ),
          onPressed: () {
            _onCapturePressed(context);
          },
        ),
      );
    } else {
      return Container(
        height: 70,
        padding: EdgeInsets.all(10),
        child: RaisedButton(
          color: Colors.white,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                Icons.repeat,
                color: Colors.black,
              ),
              Text(
                ' Foto ulang',
                style: TextStyle(
                  fontSize: 17,
                  fontFamily: "MadeTommySoft",
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(40),
            ),
          ),
          onPressed: () {
            setState(() {
              imgPath = '';
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () => FocusScope.of(context)
          .requestFocus(FocusNode()), //untuk menutup keyboard,
      child: Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            //BACKGROUND
            Container(
              height: MediaQuery.of(context).size.height / 1,
              decoration: BoxDecoration(
                // gradient: LinearGradient(
                //   colors: [Colors.blue[100], Colors.blue[800]],
                //   begin: FractionalOffset.topCenter,
                // ),
                color: Colors.blue[800],
              ),
            ),

            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //LOGO
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 6,
                    //color: Colors.lightBlue,
                    child: Image.asset(
                        "assets/images/icons/logo-arafahelectronics-white.png"),
                  ),

                  Text(
                    'Foto KTP Luar Negeri',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),

                  SizedBox(height: 10),

                  //VIEW CAMERA
                  // Container(
                  //   width: MediaQuery.of(context).size.width / 1,
                  //   height: MediaQuery.of(context).size.height / 3,
                  //   decoration: BoxDecoration(
                  //     // gradient: LinearGradient(
                  //     //   colors: [Colors.blue[100], Colors.blue[800]],
                  //     //   begin: FractionalOffset.topCenter,
                  //     // ),
                  //     color: Colors.white24,
                  //   ),
                  //   child: Center(child: _cameraPreviewWidget()),
                  // ),

                  Stack(
                    children: <Widget>[
                      Container(
                        width: size,
                        height: size,
                        child: ClipRect(
                          child: OverflowBox(
                            alignment: Alignment.center,
                            child: FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Container(
                                width: size,
                                height: size /
                                    0.56, //size / cameraController.value.aspectRatio
                                child: //Text("data"),
                                    _cameraPreviewWidget(), // this is my CameraPreview
                              ),
                            ),
                          ),
                        ),
                      ),
                      //HASIL DARI POTRET
                      Container(
                        width: size,
                        height: size,
                        child: (imgPath == '')
                            ? Container()
                            : Image.file(
                                File(imgPath),
                                fit: BoxFit.cover,
                              ),
                      ),
                      Container(
                        width: size,
                        height: size,
                        child: Image.asset(
                          "assets/images/others/frame-photo-id-card.png",
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      SizedBox(height: 100),
                    ],
                  ),

                  SizedBox(height: 5),
                  Container(
                    child: RichText(
                      text: TextSpan(
                          style: TextStyle(
                            //color: Colors.black54,
                            color: Colors.white,
                            fontSize: 15,
                          ),
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.info,
                                size: 20,
                                color: Colors.orangeAccent,
                              ),
                            ),
                            TextSpan(
                              text: ' Posisikan KTP di dalam ',
                              style: TextStyle(
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'Kotak Hijau',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.lightGreenAccent,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ]),
                    ),
                  ),

                  SizedBox(height: 30),
                  //TOMBOL CAMERA
                  widgetTombolFoto(context),
                ],
              ),
            ),

            //BUTTON NEXT
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(),
                  Row(
                    children: <Widget>[
                      // Flexible(
                      //   flex: 1,
                      //   child: Container(
                      //     height: 70,
                      //     padding: EdgeInsets.all(10),
                      //     child: RaisedButton(
                      //       color: Colors.orangeAccent,
                      //       child: Icon(
                      //         Icons.keyboard_arrow_left,
                      //         color: Colors.white,
                      //       ),
                      //       shape: RoundedRectangleBorder(
                      //         borderRadius: BorderRadius.all(
                      //           Radius.circular(30),
                      //         ),
                      //       ),
                      //       onPressed: () {
                      //         Navigator.of(context).pop();
                      //       },
                      //     ),
                      //   ),
                      // ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: RaisedButton(
                            color: Colors.orangeAccent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Lanjutkan",
                                  style: TextStyle(
                                    fontFamily: "MadeTommySoft",
                                    fontSize: 23,
                                    color: Colors.white,
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_right,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                            ),
                            onPressed: () => prosesButtonLanjutkan(context),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
