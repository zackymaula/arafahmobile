import 'dart:async';
import 'dart:io';
//import 'dart:io';

import 'package:arafahmobile/blocs/b_auth.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/main.dart';
import 'package:arafahmobile/models/m_auth.dart';
import 'package:arafahmobile/views/pages/login/page_input_detail.dart';
import 'package:arafahmobile/views/pages/login/page_input_otp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageLoginInputNomor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC AUTH CHECK APAKAH ADA ATAU TIDAK
        BlocProvider<BlocAuthenticationCheck>(
          builder: (context) => BlocAuthenticationCheck(),
        ),
      ],
      child: PageLoginInputNomorView(),
    );
  }
}

class PageLoginInputNomorView extends StatefulWidget {
  // PageLoginInputNomorView({Key key}) : super(key: key);

  @override
  _PageLoginInputNomorViewState createState() =>
      _PageLoginInputNomorViewState();
}

class _PageLoginInputNomorViewState extends State<PageLoginInputNomorView> {
  var countryNumber = '+852';
  var countryFlag = "assets/images/flags/flag_hongkong.png";
  //var textFieldName = TextEditingController();
  var textFieldPhone = TextEditingController();
  var checkStatusNomorHP = false;
  var showLoading = false;
  var progressClick = false;
  var statusCennection = false;

  Timer _timer;

  // void _onCountryChange(CountryCode countryCode) {
  //   //Todo : manipulate the selected country code here
  //   //print("New Country selected: " + countryCode.toString());
  //   countryNumber = countryCode.toString();
  //   //print(countryNumber);
  // }

  // void _encrypt() {
  //   //AES CRYPT
  //   //ENCRYPT
  //   final plainText = 'KJSoIsjdioajIOjOiJAOsidjisOIJ+space++62921292991';
  //   final key = encrypt.Key.fromUtf8(Config().key);
  //   final iv = encrypt.IV.fromLength(16);

  //   final encrypter = encrypt.Encrypter(encrypt.AES(key));

  //   final encrypted = encrypter.encrypt(plainText, iv: iv);
  //   final decrypted = encrypter.decrypt(encrypted, iv: iv);

  //   print(decrypted);
  //   print(encrypted.base64);

  //   //DECRYPT
  //   try {
  //     final keyLogin = '/DocbBMNtlujZgLth4idHmqd+f75LKuK3ppiZrxTJfdLNipxbkSs9VWKwcB3WfBBVLTjrb6Ka2oYP1bO43g06w==';
  //     final convertKeyLog = encrypt.Encrypted.fromBase64(keyLogin);
  //     final decryptKeyLog = encrypter.decrypt(convertKeyLog, iv: iv);
  //     print(decryptKeyLog);
  //   } catch (e) {
  //     print('Sorry key error ya');
  //   }
  // }

  _checkConnectionToServer() async {
    try {
      final result = await InternetAddress.lookup('m.arafah.hk');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        statusCennection = true;
      }
    } on SocketException catch (_) {
      statusCennection = false;
    }
  }

  _prosesButtonLanjutkan(context) {
    if (statusCennection == false) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Tidak ada koneksi internet.\nMohon koneksikan dengan internet dan muat ulang aplikasi',
            style: TextStyle(
              fontFamily: "MadeTommySoft",
            ),
          ),
          action: SnackBarAction(
              label: 'MUAT ULANG',
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => MyApp()),
                  (Route<dynamic> route) => false,
                );
              }),
          backgroundColor: Colors.redAccent,
          duration: Duration(seconds: 3),
        ),
      );
    } else if (textFieldPhone.text == '') {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Mohon isikan NOMOR HANDPHONE',
            style: TextStyle(
              fontFamily: "MadeTommySoft",
            ),
          ),
          backgroundColor: Colors.redAccent,
          duration: Duration(seconds: 1),
        ),
      );
      progressClick = false;
    } else if (textFieldPhone.text != '' && statusCennection == true) {
      _savePrefData();
      progressClick = true;
    }
  }

  //SET CURRENCY
  _savePrefData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(
          Config().prefmembernomorhp, countryNumber + textFieldPhone.text);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    _checkConnectionToServer();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  _actionNextPage() {
    setState(() {
      showLoading = true;
    });
    _timer = Timer(Duration(milliseconds: 2500), () {
      setState(() {
        if (checkStatusNomorHP == false) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => PageLoginInputDetail(),
            ),
          );
        } else {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) =>
                  PageLoginInputOTP(inputDataMember: false),
            ),
          );
        }
        showLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    BlocAuthenticationCheck blocauthcheck =
        BlocProvider.of<BlocAuthenticationCheck>(context);

    return GestureDetector(
      onTap: () => FocusScope.of(context)
          .requestFocus(FocusNode()), //untuk menutup keyboard
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height / 1,
              decoration: BoxDecoration(
                // gradient: LinearGradient(
                //   colors: [Colors.blue[100], Colors.blue[800]],
                //   begin: FractionalOffset.topCenter,
                // ),
                color: Colors.blue[800],
              ),
            ),
            SingleChildScrollView(
              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30),
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 3,
                    //color: Colors.lightBlue,
                    child: Image.asset(
                        "assets/images/icons/icon-arafah-logo-only.png"),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 10,
                    //color: Colors.lightBlue,
                    child: Image.asset(
                        "assets/images/icons/icon-arafah-word-only.png"),
                  ),

                  SizedBox(height: 50),

                  // Container(
                  //   margin: EdgeInsets.only(left: 20, right: 20),
                  //   padding: EdgeInsets.all(0),
                  //   decoration: BoxDecoration(
                  //     color: Colors.grey[200],
                  //     borderRadius: BorderRadius.circular(10),
                  //   ),
                  //   child: TextFormField(
                  //     //controller: textFieldName,
                  //     keyboardType: TextInputType.text,
                  //     decoration: InputDecoration(
                  //         //filled: true,
                  //         prefixIcon: Icon(
                  //           Icons.person,
                  //           color: Colors.black,
                  //         ),
                  //         labelText: "Masukkan Nama",
                  //         hintText: "Nama",
                  //         hintStyle: TextStyle(fontSize: 20),
                  //         border: InputBorder.none
                  //         // border: OutlineInputBorder(
                  //         //   borderRadius: BorderRadius.circular(10),
                  //         // ),
                  //         ),
                  //     style: TextStyle(fontSize: 20),
                  //   ),
                  // ),

                  SizedBox(height: 10),

                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    padding: EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: <Widget>[
                        // Container(
                        //   margin: EdgeInsets.only(left: 10),
                        //   //padding: EdgeInsets.all(15),
                        //   child: CountryCodePicker(
                        //     onChanged: _onCountryChange,
                        //     // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                        //     initialSelection: '+852',
                        //     //favorite: ['+852', '+886', '+65', '+60', '+62'],
                        //     // optional. Shows only country name and flag
                        //     showCountryOnly: false,
                        //     // optional. Shows only country name and flag when popup is closed.
                        //     showOnlyCountryWhenClosed: false,
                        //     // optional. aligns the flag and the Text left
                        //     countryFilter: ['HK', 'TW', 'SG', 'MY'],
                        //     alignLeft: false,
                        //     textStyle: TextStyle(fontSize: 30),
                        //     hideSearch: true,
                        //     dialogTextStyle: TextStyle(fontSize: 25),
                        //     dialogSize: Size(
                        //         MediaQuery.of(context).size.width / 1,
                        //         MediaQuery.of(context).size.height / 3),
                        //   ),
                        // ),
                        GestureDetector(
                          child: Container(
                            margin: EdgeInsets.only(left: 15),
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  countryFlag,
                                  fit: BoxFit.cover,
                                  width: 40,
                                ),
                                SizedBox(width: 10),
                                Text(
                                  countryNumber,
                                  style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "MadeTommySoft",
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            showDialog(
                              child: Dialog(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 320,
                                  padding: EdgeInsets.all(17),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Pilih Kode Negara',
                                        style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "MadeTommySoft",
                                        ),
                                      ),
                                      SizedBox(height: 20),
                                      FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            countryNumber = '+852';
                                            countryFlag =
                                                "assets/images/flags/flag_hongkong.png";
                                          });
                                          Navigator.pop(context);
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/images/flags/flag_hongkong.png",
                                              fit: BoxFit.cover,
                                              width: 50,
                                            ),
                                            SizedBox(width: 10),
                                            Text(
                                              '(+852) Hongkong',
                                              style: TextStyle(
                                                fontSize: 20,
                                                fontFamily: "MadeTommySoft",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            countryNumber = '+886';
                                            countryFlag =
                                                "assets/images/flags/flag_taiwan.png";
                                          });
                                          Navigator.pop(context);
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/images/flags/flag_taiwan.png",
                                              fit: BoxFit.cover,
                                              width: 50,
                                            ),
                                            SizedBox(width: 10),
                                            Text(
                                              '(+886) Taiwan',
                                              style: TextStyle(
                                                fontSize: 20,
                                                fontFamily: "MadeTommySoft",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            countryNumber = '+65';
                                            countryFlag =
                                                "assets/images/flags/flag_singapore.png";
                                          });
                                          Navigator.pop(context);
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/images/flags/flag_singapore.png",
                                              fit: BoxFit.cover,
                                              width: 50,
                                            ),
                                            SizedBox(width: 10),
                                            Text(
                                              '(+65) Singapore',
                                              style: TextStyle(
                                                fontSize: 20,
                                                fontFamily: "MadeTommySoft",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            countryNumber = '+60';
                                            countryFlag =
                                                "assets/images/flags/flag_malaysia.png";
                                          });
                                          Navigator.pop(context);
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/images/flags/flag_malaysia.png",
                                              fit: BoxFit.cover,
                                              width: 50,
                                            ),
                                            SizedBox(width: 10),
                                            Text(
                                              '(+60) Malaysia',
                                              style: TextStyle(
                                                fontSize: 20,
                                                fontFamily: "MadeTommySoft",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      // FlatButton(
                                      //   onPressed: () {
                                      //     setState(() {
                                      //       countryNumber = '+62';
                                      //     });
                                      //     Navigator.pop(context);
                                      //   },
                                      //   child: Row(
                                      //     children: <Widget>[
                                      //       SizedBox(width: 10),
                                      //       Text(
                                      //         '(+62) Indonesia',
                                      //         style: TextStyle(fontSize: 20),
                                      //       ),
                                      //     ],
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                              ),
                              context: context,
                            );
                          },
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: Colors.grey,
                        ),
                        Flexible(
                          child: TextFormField(
                            controller: textFieldPhone,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                                //filled: true,
                                //prefixIcon: Icon(Icons.phone_android),
                                //labelText: "Nomor Handphone",
                                hintText: "Nomor HP",
                                hintStyle: TextStyle(
                                  fontSize: 28,
                                  fontFamily: "MadeTommySoft",
                                ),
                                border: InputBorder.none
                                // border: OutlineInputBorder(
                                //   borderRadius: BorderRadius.circular(10),
                                // ),
                                ),
                            style: TextStyle(
                              fontSize: 30,
                              fontFamily: "MadeTommySoft",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 5),

                  Container(
                    child: RichText(
                      text: TextSpan(
                          style: TextStyle(
                            //color: Colors.black54,
                            color: Colors.white,
                            fontSize: 11,
                          ),
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.info,
                                size: 15,
                                color: Colors.orangeAccent,
                              ),
                            ),
                            TextSpan(
                              text: ' Pilih ',
                              style: TextStyle(
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'KODE NEGARA ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'dan masukkan ',
                              style: TextStyle(
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'NOMOR HANDPHONE',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ]),
                    ),
                  ),

                  // Container(
                  //   margin: EdgeInsets.all(20),
                  //   child: TextFormField(
                  //     obscureText: true,
                  //     decoration: InputDecoration(
                  //         filled: true,
                  //         prefixIcon: Icon(Icons.vpn_key),
                  //         labelText: "Password",
                  //         border: OutlineInputBorder(
                  //             borderRadius: BorderRadius.circular(10))),
                  //   ),
                  // ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    width: MediaQuery.of(context).size.width / 1,
                    height: 50,
                    //color: Colors.blue,
                    child: Builder(
                      builder: (BuildContext context) => RaisedButton(
                        color: Colors.orangeAccent,
                        child: (showLoading == false)
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "D A F T A R",
                                    style: TextStyle(
                                      fontFamily: "MadeTommySoft",
                                      fontSize: 23,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_right,
                                    color: Colors.white,
                                  ),
                                ],
                              )
                            : Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  backgroundColor: Colors.white,
                                ), //JIKA TIDAK ADA DATA
                              ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                        onPressed: () {
                          //prosesButtonLanjutkan(context);
                          //_checkConnectionToServer(context);
                          // if (_checkConnectionToServer(context) == true) {
                          _prosesButtonLanjutkan(context);
                          // }
                          // var check = _checkConnectionToServer(context);
                          if (progressClick == true) {
                            //   print('object');
                            try {
                              setState(() {
                                blocauthcheck.dispatch(
                                    countryNumber + textFieldPhone.text);
                                _actionNextPage();
                                progressClick = false;
                              });
                            } catch (e) {
                              print(e);
                              progressClick = false;
                            }
                            //print(countryNumber + textFieldPhone.text + 'test');
                          }
                        },
                      ),
                    ),
                  ),

                  SizedBox(height: 30),

                  BlocBuilder<BlocAuthenticationCheck, AuthenticationCheck>(
                      builder: (context, item) {
                    if (item is UninitializedAuthenticationCheck) {
                      Container();
                    } else {
                      if (item.status == 'success') {
                        checkStatusNomorHP = true;
                      } else {
                        checkStatusNomorHP = false;
                      }
                      return Container();
                    }
                    return Container();
                  }),

                  SizedBox(height: 100),

                  // SizedBox(height: 40),
                  // Text("Belum punya akun?"),
                  // SizedBox(height: 10),
                  // Container(
                  //   padding: EdgeInsets.only(left: 70, right: 70, bottom: 20),
                  //   width: MediaQuery.of(context).size.width / 1,
                  //   height: 60,
                  //   //color: Colors.blue,
                  //   child: RaisedButton(
                  //     color: Colors.blueAccent,
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: <Widget>[
                  //         // Icon(
                  //         //   Icons.flag,
                  //         //   color: Colors.white,
                  //         // ),
                  //         // SizedBox(width: 10),
                  //         Text(
                  //           "D A F T A R",
                  //           style: TextStyle(
                  //               fontWeight: FontWeight.bold,
                  //               fontSize: 17,
                  //               color: Colors.white),
                  //         ),
                  //       ],
                  //     ),
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.all(Radius.circular(30))),
                  //     onPressed: () {},
                  //   ),
                  // ),

                  // //FACEBOOK
                  // SizedBox(height: 10),
                  // //Text("Atau masuk dengan :"),
                  // Text("Login dengan :"),
                  // Container(
                  //   padding: EdgeInsets.only(
                  //       left: 60, top: 10, right: 60, bottom: 10),
                  //   width: MediaQuery.of(context).size.width / 1,
                  //   height: 80,
                  //   //color: Colors.blue,
                  //   child: RaisedButton(
                  //     color: Colors.blue[800],
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: <Widget>[
                  //         Text(
                  //           "Facebook",
                  //           style: TextStyle(
                  //               fontWeight: FontWeight.bold,
                  //               fontSize: 20,
                  //               color: Colors.white),
                  //         ),
                  //       ],
                  //     ),
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.all(Radius.circular(10))),
                  //     onPressed: () {},
                  //   ),
                  // ),

                  // //FINGER PRINT
                  // SizedBox(height: 50),
                  // //Text("Atau masuk dengan :"),
                  // Text("Atau login dengan Fingerprint"),
                  // Container(
                  //   //padding: EdgeInsets.only(left: 60, top: 10, right: 60, bottom: 10),
                  //   margin: EdgeInsets.fromLTRB(0, 20, 0, 30),
                  //   width: 100,
                  //   height: 100,
                  //   //color: Colors.blue,
                  //   child: RaisedButton(
                  //     color: Colors.blue[800],
                  //     child: Image.asset(
                  //         "assets/images/icons/fingerprint-white.png"),
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.all(Radius.circular(50))),
                  //     onPressed: () {},
                  //   ),
                  // ),

                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: <Widget>[
                  //     Text("Belum punya akun?"),
                  //     SizedBox(width: 5),
                  //     GestureDetector(
                  //       onTap: () => Navigator.of(context).push(
                  //         MaterialPageRoute(
                  //           builder: (BuildContext context) => PageUserRegist(),
                  //         ),
                  //       ),
                  //       child: Text(
                  //         "Daftar",
                  //         style: TextStyle(
                  //             fontWeight: FontWeight.bold,
                  //             decoration: TextDecoration.underline),
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  // Text(
                  //   "Lupa Password",
                  //   style: TextStyle(
                  //       fontWeight: FontWeight.bold,
                  //       decoration: TextDecoration.underline),
                  // ),
                  //SizedBox(height: 10),
                  //Text("Kirim Ulang Email Verifikasi", style: TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline),),
                  //SizedBox(height: 50),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
