import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:arafahmobile/blocs/b_auth.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/page_currency.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageLoginInputOTP extends StatelessWidget {
  var inputDataMember;

  PageLoginInputOTP({this.inputDataMember = true});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC AUTH
        BlocProvider<BlocAuthenticationRegister>(
          builder: (context) => BlocAuthenticationRegister(),
        ),
      ],
      child: PageLoginInputOTPView(inputDataMember: inputDataMember),
    );
  }
}

class PageLoginInputOTPView extends StatefulWidget {
  var inputDataMember;

  PageLoginInputOTPView({this.inputDataMember = true});

  @override
  _PageLoginInputOTPViewState createState() => _PageLoginInputOTPViewState();
}

class _PageLoginInputOTPViewState extends State<PageLoginInputOTPView> {
  // final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  // Position _currentPosition;
  // String _currentAddress;

  var membernomorhp = '';
  var datamember = ''; // ALL DATA PREF

  String smsOTP;
  String verificationId;
  String errorMessage = '';
  FirebaseAuth _auth = FirebaseAuth.instance;

  //GET PREF
  Future<Null> _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    membernomorhp = pref.getString(Config().prefmembernomorhp) ?? "";
    var membernamadepan = pref.getString(Config().prefmembernamadepan) ?? "";
    var membernamabelakang =
        pref.getString(Config().prefmembernamabelakang) ?? "";
    var membertgllahir = pref.getString(Config().prefmembertgllahir) ?? "";
    var memberphotoidcard =
        pref.getString(Config().prefmemberphotoidcard) ?? "";
    var memberfacebookname =
        pref.getString(Config().prefmemberfacebookname) ?? "";
    var memberfacebookemail =
        pref.getString(Config().prefmemberfacebookemail) ?? "";
    var memberfacebookphone =
        pref.getString(Config().prefmemberfacebookphone) ?? "";
    var memberfacebookphoto =
        pref.getString(Config().prefmemberfacebookphoto) ?? "";
    var memberlocationurl =
        pref.getString(Config().prefmemberlocationurl) ?? "";
    var memberlocationfull =
        pref.getString(Config().prefmemberlocationfull) ?? "";
    datamember = membernomorhp +
        '+space+' +
        membernamadepan +
        '+space+' +
        membernamabelakang +
        '+space+' +
        membertgllahir +
        '+space+' +
        memberphotoidcard +
        '+space+' +
        memberfacebookname +
        '+space+' +
        memberfacebookemail +
        '+space+' +
        memberfacebookphone +
        '+space+' +
        memberfacebookphoto +
        '+space+' +
        memberlocationurl +
        '+space+' +
        memberlocationfull;
    verifyPhone();
  }

  //KIRIM OTP
  Future<void> verifyPhone() async {
    final PhoneCodeSent smsOTPSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
    };
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: membernomorhp, // PHONE NUMBER TO SEND OTP
          codeAutoRetrievalTimeout: (String verId) {
            //Starts the phone number verification process for the given phone number.
            //Either sends an SMS with a 6 digit code to the phone number specified, or sign's the user in and [verificationCompleted] is called.
            this.verificationId = verId;
          },
          codeSent:
              smsOTPSent, // WHEN CODE SENT THEN WE OPEN DIALOG TO ENTER OTP.
          timeout: const Duration(seconds: 20),
          verificationCompleted: (AuthCredential phoneAuthCredential) {
            print(phoneAuthCredential);
          },
          verificationFailed: (AuthException exceptio) {
            print('${exceptio.message}');
          });
    } catch (e) {
      handleError(e);
    }
  }

  //CHECKING OTP
  signIn() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verificationId,
        smsCode: smsOTP,
      );
      //JIKA OTP COCOK
      final FirebaseUser user =
          (await _auth.signInWithCredential(credential)).user;
      final FirebaseUser currentUser = await _auth.currentUser();
      assert(user.uid == currentUser.uid);

      //ENCRYPT (UID +space+ Phone)
      final _key = _encryptKey(user.uid, membernomorhp);

      //INSERT DATA MEMEBER TO API
      if (widget.inputDataMember == true) {
        _insertMemberToAPI(user.uid);
      }

      //SAVE SHARE PREF KEY ENCRYPT
      _savePrefData(_key);

      //MOVE TO PAGE MAIN
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => PageCurrency()),
        (Route<dynamic> route) => false,
      );
    } catch (e) {
      handleError(e);
    }
  }

  //JIKA OTP ERROR
  handleError(PlatformException error) {
    print(error);
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          errorMessage = 'OTP tidak cocok';
        });
        break;
      default:
        setState(() {
          errorMessage = error.message;
        });

        break;
    }
  }

  //ENCRYPT (UID +space+ Phone)
  _encryptKey(String uid, String phone) {
    final plainText = uid + '+space+' + phone;
    final key = encrypt.Key.fromUtf8(Config().key);
    final iv = encrypt.IV.fromLength(16);
    final encrypter = encrypt.Encrypter(encrypt.AES(key));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    return encrypted.base64;
  }

  //TIMER COUNTDOWN
  Timer _timer;
  int _start = 120;

  //CONVERT SECONT TO TIME CLOCK
  String get convertToClock {
    Duration duration = Duration(seconds: _start);
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  //START COUNTDOWN
  void startTimer() {
    _start = 120;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  //INSERT DATA MEMBER INTO API
  _insertMemberToAPI(String uidFirebase) {
    BlocAuthenticationRegister blocauthregister =
        BlocProvider.of<BlocAuthenticationRegister>(context);
    try {
      blocauthregister.dispatch(uidFirebase + '+space+' + datamember);
    } catch (e) {
      print(e);
    }
  }

  //SAVE SHARE PREF KEY ENCRYPT and CLEAR DATA PREF
  _savePrefData(String memberkeylog) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberkeylog, memberkeylog);
      pref.setString(Config().prefmembernomorhp, '');
      pref.setString(Config().prefmembernamadepan, '');
      pref.setString(Config().prefmembernamabelakang, '');
      pref.setString(Config().prefmembertgllahir, '');
      pref.setString(Config().prefmemberphotoidcard, '');
      pref.setString(Config().prefmemberfacebookname, '');
      pref.setString(Config().prefmemberfacebookemail, '');
      pref.setString(Config().prefmemberfacebookphone, '');
      pref.setString(Config().prefmemberfacebookphoto, '');
      pref.setString(Config().prefmemberlocationurl, '');
      pref.setString(Config().prefmemberlocationfull, '');
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    _getPref();
    //verifyPhone();
    startTimer();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // BlocAuthenticationRegister blocauthregister = BlocProvider.of<BlocAuthenticationRegister>(context);
    // try {
    //   blocauthregister.dispatch(_decryptKeyReturn +
    //       '+space+' +
    //       _memberNamaDepan +
    //       '+space+' +
    //       _memberLocationUrl +
    //       '+space+' +
    //       _memberLocationFull);
    // } catch (e) {
    //   print(e);
    // }

    return GestureDetector(
      onTap: () => FocusScope.of(context)
          .requestFocus(FocusNode()), //untuk menutup keyboard,
      child: Scaffold(
        // appBar: AppBar(
        //   title: Image.asset(
        //     "assets/images/icons/logo-arafahelectronics.png",
        //     fit: BoxFit.cover,
        //     height: 40,
        //   ),
        //   backgroundColor: Colors.white,
        //   centerTitle: true,
        //   iconTheme: IconThemeData(color: Colors.blueAccent),
        // ),
        body: Stack(
          children: <Widget>[
            //BACKGROUND
            // Container(
            //   height: MediaQuery.of(context).size.height / 1,
            //   decoration: BoxDecoration(
            //     gradient: LinearGradient(
            //       colors: [Colors.blue[800], Colors.blue[100]],
            //       begin: FractionalOffset.topCenter,
            //     ),
            //   ),
            // ),

            //CONTENT
            SingleChildScrollView(
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 50),

                    //LOGO
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 7,
                      //color: Colors.lightBlue,
                      child: Image.asset(
                          "assets/images/icons/logo-arafahelectronics.png"),
                    ),

                    SizedBox(height: 30),
                    //NOMOR
                    Container(
                      child: Text(
                        membernomorhp,
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue[900],
                          fontFamily: "MadeTommySoft",
                        ),
                      ),
                    ),
                    SizedBox(height: 30),

                    //Image.memory(_bytes),

                    //INFORMASI
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: RichText(
                        text: TextSpan(
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                          ),
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.info,
                                size: 27,
                                color: Colors.orange,
                              ),
                            ),
                            TextSpan(
                              text: ' Masukkan ',
                              style: TextStyle(
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'Kode OTP ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue[900],
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'yang dikirimkan melalui ',
                              style: TextStyle(
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'SMS ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue[900],
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            TextSpan(
                              text: 'ke nomor handphone tersebut',
                              style: TextStyle(
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20),

                    //INPUT OTP
                    Container(
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: PinEntryTextField(
                          showFieldAsBox: true,
                          fields: 6,
                          onSubmit: (String otp) {
                            setState(() {
                              this.smsOTP = otp;
                            });
                            signIn();

                            // CHECKING AUTH DI INTERNAL
                            // _auth.currentUser().then((user) {
                            //   if (user != null) {
                            //     Navigator.of(context).pushAndRemoveUntil(
                            //       MaterialPageRoute(
                            //         builder: (BuildContext context) => PageMain(
                            //           selectedPage: 3,
                            //           loginStatus: true,
                            //         ),
                            //       ),
                            //       (Route<dynamic> route) => false,
                            //     );
                            //     print(user.uid + 'down user uid');
                            //   } else {
                            //     signIn();
                            //   }
                            // });
                          },
                        ),
                      ),
                    ),
                    (errorMessage != ''
                        ? Text(
                            errorMessage,
                            style: TextStyle(color: Colors.red),
                          )
                        : Container()),

                    //SizedBox(height: 130),
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 150,
                      child: VerticalDivider(color: Colors.grey),
                    ),

                    //INFORMASI KIRIM ULANG
                    Text(
                      'Kode OTP belum terkirim?',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                    Text(
                      'Tunggu sebentar untuk kirim ulang OTP',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      '$convertToClock',
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.blue[900],
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                    SizedBox(height: 10),

                    //BUTTON KIRIM ULANG
                    Container(
                      //padding: EdgeInsets.only(left: 70, right: 70),
                      //width: MediaQuery.of(context).size.width / 1,
                      height: 40,
                      //color: Colors.blue,
                      child: RaisedButton(
                        color: Colors.blue[900],
                        child: Text(
                          "   KIRIM ULANG OTP   ",
                          style: TextStyle(
                            //fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.white, fontFamily: "MadeTommySoft",
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                        onPressed: (_start > 0)
                            ? null
                            : () {
                                verifyPhone(); //SENT OTP ULANG
                                startTimer();
                              },
                      ),
                    ),

                    SizedBox(height: 100),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
