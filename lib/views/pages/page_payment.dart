import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:flutter/material.dart';

class PagePayment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false, //DISABLE BACK BUTTON
      child: MaterialApp(
        debugShowCheckedModeBanner: false, //DISABLE BANNER DEBUG
        home: Scaffold(
          appBar: AppBar(
            // title: Text(
            //   productLabel,
            //   style: TextStyle(
            //     color: Colors.blueAccent,
            //     fontWeight: FontWeight.bold,
            //   ),
            // ),
            title: Image.asset(
              "assets/images/icons/logo-arafahelectronics.png",
              fit: BoxFit.cover,
              height: 40,
            ),
            backgroundColor: Colors.white,
            centerTitle: true,
            iconTheme: IconThemeData(color: Colors.blueAccent),
          ),
          body: Container(
            height: MediaQuery.of(context).size.height / 1,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.orange[100], Colors.blue[100]],
                begin: FractionalOffset.topCenter,
              ),
            ),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  //TUNGGU 1X24
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 6,
                          offset: Offset(1, 1),
                        ),
                      ],
                    ),
                    child: Text(
                      "Pesanan akan kami proses, \n silahkan tunggu 1x24, \n kami akan menghubungi Anda \n untuk proses selanjutnya",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "atau",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),

                  //HUBUNGI KAMI
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 6,
                          offset: Offset(1, 1),
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Dapat langsung menghubungi kami di :",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 18),
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.call),
                            Text(
                              " 7897-8973",
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.call),
                            Text(
                              " 7897-8973",
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.call),
                            Text(
                              " 7897-8973",
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  
                  //BUTTON KEMBALI KE BERANDA
                  RaisedButton(
                    elevation: 3,
                    child: Text(
                      "Kembali ke Beranda",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                          builder: (BuildContext context) => PageMain(),
                        ),
                        (Route<dynamic> route) => false,
                      );
                    },
                    color: Colors.orangeAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
