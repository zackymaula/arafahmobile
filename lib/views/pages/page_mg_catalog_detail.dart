import 'package:arafahmobile/blocs/b_mg_cart.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/login/page_input_nomor.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

class PageMGCatalogDetail extends StatelessWidget {
  String id;
  String name;
  String sku;
  String price;
  String description;
  String image;
  String categorynames;
  String categorysubnames;

  PageMGCatalogDetail(
      {this.id,
      this.name,
      this.sku,
      this.price,
      this.description,
      this.image,
      this.categorynames,
      this.categorysubnames});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC CART INSERT (MG)
        BlocProvider<BlocMGCartInsert>(
          builder: (context) => BlocMGCartInsert(),
        ),
      ],
      child: PageMGCatalogDetailView(
        id: id,
        name: name,
        sku: sku,
        price: price,
        description: description,
        image: image,
        categorynames: categorynames,
        categorysubnames: categorysubnames,
      ),
    );
  }
}

class PageMGCatalogDetailView extends StatefulWidget {
  String id;
  String name;
  String sku;
  String price;
  String description;
  String image;
  String categorynames;
  String categorysubnames;

  PageMGCatalogDetailView(
      {this.id,
      this.name,
      this.sku,
      this.price,
      this.description,
      this.image,
      this.categorynames,
      this.categorysubnames});

  @override
  _PageMGCatalogDetailViewState createState() =>
      _PageMGCatalogDetailViewState();
}

class _PageMGCatalogDetailViewState extends State<PageMGCatalogDetailView> {
  //var _statusLogin = false;
  var _decryptKeyReturn;
  String _price = '';

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  //CHECK KEY AUTH SHARE PREF > JIKA KOSONG LOGIN SET FALSE
  _getPrefKeyLog() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefmemberkeylog) ?? "";
  }

  //DECRYPT KEY > JIKA GAGAL STATUS LOGIN SET FALSE
  _decryptKey(String keylog) {
    try {
      final key = encrypt.Key.fromUtf8(Config().key);
      final iv = encrypt.IV.fromLength(16);
      final encrypter = encrypt.Encrypter(encrypt.AES(key));

      final convertkeylog = encrypt.Encrypted.fromBase64(keylog);
      _decryptKeyReturn = encrypter.decrypt(convertkeylog, iv: iv);

      setState(() {});
    } catch (e) {
      //JIKA DECRYPT GAGAL > LOGIN SET FALSE
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => PageLoginInputNomor()),
        (Route<dynamic> route) => false,
      );
      setState(() {});
    }
  }

  @override
  void initState() {
    _getPrefKeyLog().then((k) {
      _decryptKey(k);
      setState(() {});
    });

    _getPrefCurrency().then((i) {
      var price = int.parse(widget.price);

      if (i == 'HKD') {
        _price = (price * 1).toString() + ' ' + i;
      } else if (i == 'NTD') {
        _price = (price * 4).toString() + ' ' + i;
      } else if (i == 'SGD') {
        _price = (price * 0.2).toString() + ' ' + i;
      } else if (i == 'MYR') {
        _price = (price * 0.6).toString() + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics-white.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Stack(
        children: <Widget>[
          //KONTEN
          Container(
            height: MediaQuery.of(context).size.height / 1,
            width: MediaQuery.of(context).size.width / 1,
            color: Colors.grey[200],
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //KONTEN GAMBAR
                  Container(
                    padding: EdgeInsets.all(15),
                    height: 350,
                    width: MediaQuery.of(context).size.width / 1,
                    color: Colors.white,
                    child: CachedNetworkImage(
                      imageUrl: Config().mgpathimage + widget.image,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          backgroundColor: Colors.orangeAccent,
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  SizedBox(height: 10),
                  //KONTEN NAMA DAN HARGA
                  Container(
                    padding: EdgeInsets.all(15),
                    width: MediaQuery.of(context).size.width / 1,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.name,
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          _price,
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.blue[800],
                            fontWeight: FontWeight.bold,
                            fontFamily: "MadeTommySoft",
                          ),
                        )
                        // (widget.productPriceDiscount != '')
                        //     ? Row(
                        //         children: <Widget>[
                        //           Text(
                        //             widget.productPrice,
                        //             style: TextStyle(
                        //               fontSize: 17,
                        //               color: Colors.grey,
                        //               //fontWeight: FontWeight.bold,
                        //               decoration: TextDecoration.lineThrough,
                        //             ),
                        //           ),
                        //           SizedBox(width: 5),
                        //           Text(
                        //             widget.productPriceDiscount,
                        //             style: TextStyle(
                        //                 fontSize: 20,
                        //                 color: Colors.red,
                        //                 fontWeight: FontWeight.bold),
                        //           ),
                        //         ],
                        //       )
                        //     : Text(
                        //         widget.productPrice,
                        //         style: TextStyle(
                        //             fontSize: 20,
                        //             color: Colors.blue,
                        //             fontWeight: FontWeight.bold),
                        //       )
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  //KONTEN DESKRIPSI
                  Container(
                    padding: EdgeInsets.all(15),
                    width: MediaQuery.of(context).size.width / 1,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Deskripsi Produk",
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        SizedBox(height: 10),
                        //HtmlWidget(widget.productDescription)
                        Text(
                          widget.description,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 70),
                ],
              ),
            ),
          ),

          //TOMBOL TAMBAH KERANJANG
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(),
                Container(
                  height: 70,
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width / 1,
                  child: RaisedButton(
                    color: Colors.orangeAccent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Tambah ke Keranjang ",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        Icon(
                          Icons.add_shopping_cart,
                          color: Colors.white,
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    onPressed: () {
                      _showDialog(context);
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _showDialog(context) {
    BlocMGCartInsert blocmgcartinsert =
        BlocProvider.of<BlocMGCartInsert>(context);

    blocmgcartinsert.dispatch(_decryptKeyReturn +
        '+space+' +
        widget.id +
        '+space+' +
        widget.name +
        '+space+' +
        widget.sku +
        '+space+' +
        widget.price +
        '+space+' +
        widget.description +
        '+space+' +
        widget.image +
        '+space+' +
        widget.categorynames +
        '+space+' +
        widget.categorysubnames);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 300,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Icon(
                    Icons.beenhere,
                    color: Colors.green,
                    size: 70,
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Produk sudah ditambahkan ke keranjang',
                    style: TextStyle(
                      fontSize: 25,
                      fontFamily: "MadeTommySoft",
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          'Lanjut Belanja',
                          style: TextStyle(
                            color: Colors.blue[800],
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Spacer(),
                      FlatButton(
                        child: Text(
                          'Lihat ke Keranjang',
                          style: TextStyle(
                            color: Colors.blue[800],
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PageMain(selectedPage: 2),
                            ),
                            (Route<dynamic> route) => false,
                          );
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
