import 'package:arafahmobile/blocs/b_category_sub.dart';
import 'package:arafahmobile/blocs/b_mg_catalog_categorysub.dart';
import 'package:arafahmobile/blocs/b_product_categorysub.dart';
import 'package:arafahmobile/models/m_category_sub.dart';
import 'package:arafahmobile/models/m_mg_catalog_categorysub.dart';
import 'package:arafahmobile/models/m_product_categorysub.dart';
import 'package:arafahmobile/views/components/component_category_sub.dart';
import 'package:arafahmobile/views/components/component_mg_catalog_categorysub.dart';
import 'package:arafahmobile/views/components/component_product_categorysub.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PageCategorySubProductList extends StatelessWidget {
  final int categoryId;
  final int categorySubId;
  final String categoryName;
  final String categorySubName;
  final String categoryImage;

  PageCategorySubProductList(
      {this.categoryId,
      this.categorySubId,
      this.categoryName,
      this.categorySubName,
      this.categoryImage});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC CATEGORY SUB
        BlocProvider<BlocCategorySubList>(
          builder: (context) => BlocCategorySubList(),
        ),
        //BLOC PRODUCT CATEGORYSUB
        // BlocProvider<BlocProductListCategorySub>(
        //   builder: (context) => BlocProductListCategorySub(),
        // ),
        //BLOC PRODUCT CATEGORYSUB MAGENTO
        BlocProvider<BlocMGCatalogCategorySub>(
          builder: (context) => BlocMGCatalogCategorySub(),
        ),
      ],
      child: PageCategorySubProductListView(
        categoryId: categoryId,
        categorySubId: categorySubId,
        categoryName: categoryName,
        categorySubName: categorySubName,
        categoryImage: categoryImage,
      ),
    );
  }
}

class PageCategorySubProductListView extends StatelessWidget {
  final int categoryId;
  final int categorySubId;
  final String categoryName;
  final String categorySubName;
  final String categoryImage;

  PageCategorySubProductListView(
      {this.categoryId,
      this.categorySubId,
      this.categoryName,
      this.categorySubName,
      this.categoryImage});

  @override
  Widget build(BuildContext context) {
    BlocCategorySubList bloccategorylist =
        BlocProvider.of<BlocCategorySubList>(context);
    bloccategorylist.dispatch(categoryId);
    // BlocProductListCategorySub blocproductcategorysub =
    //     BlocProvider.of<BlocProductListCategorySub>(context);
    // blocproductcategorysub
    //     .dispatch(categoryId.toString() + '/' + categorySubId.toString());
    BlocMGCatalogCategorySub blocmgcatalogcategorysub =
        BlocProvider.of<BlocMGCatalogCategorySub>(context);
    blocmgcatalogcategorysub
        .dispatch(categoryName + '+space+' + categorySubName);

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text(
              categoryName,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ],
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        color: Colors.grey[200],
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //HEADER
              Container(
                height: 70,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                //color: Color(int.parse(_categoryColor[index])),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blue[800],
                      Colors.blue[200],
                    ],
                  ),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(50)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(1),
                      blurRadius: 5,
                      offset: Offset(2, 2),
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      categorySubName,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 20),

              //SUB KATEGORI
              Container(
                width: MediaQuery.of(context).size.width / 1,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 6,
                      child: BlocBuilder<BlocCategorySubList,
                          List<CategorySubList>>(
                        builder: (context, itemlist) =>
                            (itemlist is List<UninitializedCategorySubList>)
                                ? Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                      backgroundColor: Colors.orangeAccent,
                                    ),
                                  )
                                : ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: itemlist.length,
                                    itemBuilder: (context, index) =>
                                        ComponentCategorySub(itemlist[index]),
                                  ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 10),
              Divider(color: Colors.grey),
              SizedBox(height: 10),

              //PRODUCT-PRODUCT
              // Container(
              //   margin: EdgeInsets.only(left: 5, right: 5),
              //   width: MediaQuery.of(context).size.width / 1,
              //   //color: Colors.yellow[200],
              //   child: Column(
              //     children: <Widget>[
              //       SizedBox(
              //         child: BlocBuilder<BlocProductListCategorySub,
              //             List<ProductListCategorySub>>(
              //           builder: (context, itemlist) => (itemlist
              //                   is List<UninitializedProductListCategorySub>)
              //               ? Center(
              //                   child: CircularProgressIndicator(),
              //                 )
              //               : GridView.builder(
              //                   gridDelegate:
              //                       SliverGridDelegateWithFixedCrossAxisCount(
              //                           crossAxisCount: 2,
              //                           childAspectRatio: 2.3 / 3),
              //                   physics: ClampingScrollPhysics(),
              //                   shrinkWrap: true,
              //                   itemCount: itemlist.length,
              //                   itemBuilder: (context, index) =>
              //                       ComponentProductCategorySub(
              //                           itemlist[index]),
              //                 ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),

              //PRODUCT-PRODUCT FROM MAGENTO
              Container(
                margin: EdgeInsets.only(left: 5, right: 5),
                width: MediaQuery.of(context).size.width / 1,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      child: BlocBuilder<BlocMGCatalogCategorySub,
                          List<MGCatalogCategorySub>>(
                        builder: (context, itemlist) => (itemlist
                                is List<UninitializedMGCatalogCategorySub>)
                            ? Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  backgroundColor: Colors.orangeAccent,
                                ),
                              )
                            : GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        childAspectRatio: 2.2 / 3),
                                physics: ClampingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: itemlist.length,
                                itemBuilder: (context, index) =>
                                    ComponentMGCatalogCategorySub(
                                        itemlist[index]),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
