import 'package:arafahmobile/blocs/b_cart.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/page_cart.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

class PageProductDetail extends StatelessWidget {
  final String idproduct;
  final String idpromo;
  final String productLabel;
  final String productImage;
  final String productPrice;
  final String productPriceDiscount;
  final String productDescription;

  PageProductDetail(
      {this.idproduct = "",
      this.idpromo = "",
      this.productLabel = "",
      this.productImage = "",
      this.productPrice = "",
      this.productPriceDiscount,
      this.productDescription = ""});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC AUTH
        BlocProvider<BlocCartInsert>(
          builder: (context) => BlocCartInsert(),
        ),
      ],
      child: PageProductDetailView(
        idproduct: idproduct,
        idpromo: idpromo,
        productLabel: productLabel,
        productImage: productImage,
        productPrice: productPrice,
        productPriceDiscount: productPriceDiscount,
        productDescription: productDescription,
      ),
    );
  }
}

class PageProductDetailView extends StatefulWidget {
  final String idproduct;
  final String idpromo;
  final String productLabel;
  final String productImage;
  final String productPrice;
  final String productPriceDiscount;
  final String productDescription;

  PageProductDetailView(
      {this.idproduct = "",
      this.idpromo = "",
      this.productLabel = "",
      this.productImage = "",
      this.productPrice = "",
      this.productPriceDiscount,
      this.productDescription = ""});

  @override
  _PageProductDetailViewState createState() => _PageProductDetailViewState();
}

class _PageProductDetailViewState extends State<PageProductDetailView> {
  var _statusLogin = false;
  var _decryptKeyReturn;

  //CHECK KEY AUTH SHARE PREF > JIKA KOSONG LOGIN SET FALSE
  _getPrefKeyLog() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefmemberkeylog) ?? "";
  }

  //DECRYPT KEY > JIKA GAGAL STATUS LOGIN SET FALSE
  _decryptKey(String keylog) {
    try {
      final key = encrypt.Key.fromUtf8(Config().key);
      final iv = encrypt.IV.fromLength(16);
      final encrypter = encrypt.Encrypter(encrypt.AES(key));

      final convertkeylog = encrypt.Encrypted.fromBase64(keylog);
      _decryptKeyReturn = encrypter.decrypt(convertkeylog, iv: iv);
      _statusLogin = true;

      setState(() {});
    } catch (e) {
      //JIKA DECRYPT GAGAL > LOGIN SET FALSE
      _statusLogin = false;
      setState(() {});
    }
  }

  @override
  void initState() {
    _getPrefKeyLog().then((k) {
      (k == '') ? _statusLogin = false : _decryptKey(k);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.blueAccent),
      ),
      body: Stack(
        children: <Widget>[
          //KONTEN
          Container(
            height: MediaQuery.of(context).size.height / 1,
            width: MediaQuery.of(context).size.width / 1,
            color: Colors.grey[200],
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //KONTEN GAMBAR
                  Container(
                    padding: EdgeInsets.all(15),
                    height: 350,
                    width: MediaQuery.of(context).size.width / 1,
                    color: Colors.white,
                    child: CachedNetworkImage(
                      imageUrl: widget.productImage,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          backgroundColor: Colors.orangeAccent,
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  SizedBox(height: 10),
                  //KONTEN NAMA DAN HARGA
                  Container(
                    padding: EdgeInsets.all(15),
                    width: MediaQuery.of(context).size.width / 1,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.productLabel,
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10),
                        (widget.productPriceDiscount != '')
                            ? Row(
                                children: <Widget>[
                                  Text(
                                    widget.productPrice,
                                    style: TextStyle(
                                      fontSize: 17,
                                      color: Colors.grey,
                                      //fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    widget.productPriceDiscount,
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )
                            : Text(
                                widget.productPrice,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold),
                              )
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  //KONTEN DESKRIPSI
                  Container(
                    padding: EdgeInsets.all(15),
                    width: MediaQuery.of(context).size.width / 1,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Deskripsi Produk",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10),
                        HtmlWidget(widget.productDescription)
                      ],
                    ),
                  ),
                  SizedBox(height: 70),
                ],
              ),
            ),
          ),

          //TOMBOL TAMBAH KERANJANG
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(),
                Container(
                  height: 70,
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width / 1,
                  child: RaisedButton(
                    color: Colors.orangeAccent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          "Tambah ke Keranjang",
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                        Icon(
                          Icons.add_shopping_cart,
                          color: Colors.white,
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    onPressed: () {
                      _showDialog(context);
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _showDialog(context) {
    BlocCartInsert bloccartinsert = BlocProvider.of<BlocCartInsert>(context);

    if (_statusLogin == false) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 300,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Icon(
                      Icons.info,
                      color: Colors.orangeAccent,
                      size: 70,
                    ),
                    SizedBox(height: 20),
                    Text(
                      'LOGIN dahulu untuk menambahkan ke keranjang',
                      style: TextStyle(fontSize: 25),
                      textAlign: TextAlign.center,
                    ),
                    Spacer(),
                    Row(
                      children: <Widget>[
                        FlatButton(
                          child: Text(
                            'Lanjut Belanja',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Spacer(),
                        FlatButton(
                          child: Text(
                            'LOGIN',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                          onPressed: () {
                            Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageMain(selectedPage: 3),
                              ),
                              (Route<dynamic> route) => false,
                            );
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
      );
    } else {
      bloccartinsert.dispatch(_decryptKeyReturn +
          '+space+' +
          widget.idproduct +
          '+space+' +
          widget.idpromo); //generateid + phone + idproduct + idpromo

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 300,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Icon(
                      Icons.beenhere,
                      color: Colors.green,
                      size: 70,
                    ),
                    SizedBox(height: 20),
                    Text(
                      'Produk sudah ditambahkan ke keranjang',
                      style: TextStyle(fontSize: 25),
                      textAlign: TextAlign.center,
                    ),
                    Spacer(),
                    Row(
                      children: <Widget>[
                        FlatButton(
                          child: Text(
                            'Lanjut Belanja',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Spacer(),
                        FlatButton(
                          child: Text(
                            'Lihat ke Keranjang',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                          onPressed: () {
                            Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageMain(selectedPage: 2),
                              ),
                              (Route<dynamic> route) => false,
                            );
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
      );
    }
  }
}
