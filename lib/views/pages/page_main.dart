import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/login/page_input_nomor.dart';
import 'package:arafahmobile/views/pages/page_cart.dart';
import 'package:arafahmobile/views/pages/page_category.dart';
import 'package:arafahmobile/views/pages/page_home.dart';
import 'package:arafahmobile/views/pages/page_menu.dart';
import 'package:arafahmobile/views/pages/page_user_main.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageMain extends StatefulWidget {
  int selectedPage;
  //bool loginStatus; //PENANDA LOGIN

  PageMain({this.selectedPage = 0}); //, this.loginStatus = false

  @override
  _PageMainState createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;

  final Key keyHome = PageStorageKey('pageHome');
  final Key keyCategory = PageStorageKey('pageCategory');
  final Key keyCart = PageStorageKey('pageCart');
  //final Key keyUserLogin = PageStorageKey('pageUserLogin');
  final Key keyUserMain = PageStorageKey('pageUserMain');
  final Key keyMenu = PageStorageKey('pageMenu');

  List _pageOptions;

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  void initState() {
    _getCurrentLocation();
    _pageOptions = [
      PageHome(key: keyHome),
      PageCategory(key: keyCategory),
      PageCart(key: keyCart),
      PageUserMain(key: keyUserMain),
      PageMenu(key: keyMenu)
    ];
    super.initState();
  }

  // _pageOptionLogin() {
  //   if (widget.loginStatus == false) {
  //     return PageLoginInputNomor(key: keyUserLogin);
  //   } else {
  //     return PageUserMain(key: keyUserMain);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        bucket: bucket,
        child: _pageOptions[widget.selectedPage],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: widget.selectedPage,
        onTap: (int index) {
          setState(() {
            widget.selectedPage = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            backgroundColor: Colors.blue[900],
            icon: Icon(Icons.local_mall),
            activeIcon:
                Icon(Icons.local_mall, color: Colors.orangeAccent, size: 30),
            title: Text(
              "Toko",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.blue[900],
            icon: Icon(Icons.style),
            activeIcon: Icon(Icons.style, color: Colors.orangeAccent, size: 30),
            title: Text(
              "Kategori",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.blue[900],
            icon: Icon(Icons.shopping_cart),
            activeIcon:
                Icon(Icons.shopping_cart, color: Colors.orangeAccent, size: 30),
            title: Text(
              "Keranjang",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.blue[900],
            icon: Icon(Icons.person_pin),
            activeIcon:
                Icon(Icons.person_pin, color: Colors.orangeAccent, size: 30),
            title: Text(
              "Akun",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.blue[900],
            icon: Icon(Icons.bubble_chart),
            activeIcon:
                Icon(Icons.bubble_chart, color: Colors.orangeAccent, size: 30),
            title: Text(
              "Menu",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ),
        ],
      ),
    );
  }

  //GET LOCATION LAT LONG
  _getCurrentLocation() async {
    //final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    try {
      await geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) {
        setState(() {
          _currentPosition = position;
        });

        _getAddressFromLatLng();
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }
  }

  //GET LOCATION FULL
  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.thoroughfare}, ${place.subThoroughfare}, ${place.subLocality}, ${place.locality}, ${place.subAdministrativeArea}, ${place.administrativeArea}, ${place.country}, ${place.postalCode}";

        //print(_currentPosition);
        //print(_currentAddress);
        _savePrefData();
      });
    } catch (e) {
      print(e);
    }
  }

  //SET LOCATION TO PREF
  _savePrefData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberlocationurl,
          'https://www.google.co.id/maps/@${_currentPosition.latitude.toString()},${_currentPosition.longitude.toString()},20z');
      pref.setString(
          Config().prefmemberlocationfull, _currentAddress.toString());
    });
  }
}
