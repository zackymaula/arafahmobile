import 'dart:io';

import 'package:arafahmobile/blocs/b_cart.dart';
import 'package:arafahmobile/blocs/b_mg_cart.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_cart.dart';
import 'package:arafahmobile/models/m_mg_cart.dart';
import 'package:arafahmobile/views/components/component_cart.dart';
import 'package:arafahmobile/views/components/component_mg_cart.dart';
import 'package:arafahmobile/views/pages/login/page_input_nomor.dart';
import 'package:arafahmobile/views/pages/page_cart_confirm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:url_launcher/url_launcher.dart';

class PageCart extends StatelessWidget {
  PageCart({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC CART LIST
        // BlocProvider<BlocCartList>(
        //   builder: (context) => BlocCartList(),
        // ),
        //BLOC CART LIST
        BlocProvider<BlocMGCartList>(
          builder: (context) => BlocMGCartList(),
        ),
      ],
      child: PageCartView(),
    );
  }
}

class PageCartView extends StatefulWidget {
  @override
  _PageCartViewState createState() => _PageCartViewState();
}

class _PageCartViewState extends State<PageCartView> {
  //var _statusLogin = false;
  var _decryptKeyReturn;
  //var _memberName;

  //CHECK KEY AUTH SHARE PREF > JIKA KOSONG LOGIN SET FALSE
  _getPrefKeyLog() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    //_memberName = pref.getString(Config().prefmembername) ?? "";
    return pref.getString(Config().prefmemberkeylog) ?? "";
  }

  //DECRYPT KEY > JIKA GAGAL STATUS LOGIN SET FALSE
  _decryptKey(String keylog) {
    try {
      final key = encrypt.Key.fromUtf8(Config().key);
      final iv = encrypt.IV.fromLength(16);
      final encrypter = encrypt.Encrypter(encrypt.AES(key));

      final convertkeylog = encrypt.Encrypted.fromBase64(keylog);
      _decryptKeyReturn = encrypter.decrypt(convertkeylog, iv: iv);
      //_statusLogin = true;

      setState(() {});
    } catch (e) {
      //JIKA DECRYPT GAGAL > LOGIN SET FALSE
      //_statusLogin = false;
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => PageLoginInputNomor()),
        (Route<dynamic> route) => false,
      );
      setState(() {});
    }
  }

  // _getMessageWhatsAppPlatform() {
  //   var phone = '85257039400'; //NOMOR ARAFAH
  //   var text =
  //       'Hi ARAFAH, %0A%0ASaya sudah memasukkan produk ke keranjang My Arafah. Mohon dibantu bagaimana cara pembayarannya. %0A%0ATerima kasih';
  //   if (Platform.isIOS) {
  //     return 'whatsapp://send?phone=$phone';
  //   } else if (Platform.isAndroid) {
  //     return 'whatsapp://send?phone=$phone&text=$text';
  //   }
  //   return null;
  // }

  // _getMessagePlatform() {
  //   var phone = '+85257039400'; //NOMOR ARAFAH
  //   var text =
  //       'Hi ARAFAH, %0A%0ASaya sudah memasukkan produk ke keranjang My Arafah. Mohon dibantu bagaimana cara pembayarannya. %0A%0ATerima kasih';
  //   if (Platform.isIOS) {
  //     return 'sms:$phone';
  //   } else if (Platform.isAndroid) {
  //     return 'sms:$phone?text=$text';
  //   }
  //   return null;
  // }

  // _launchWhatsApp(context) async {
  //   var whatsappUrl = _getMessageWhatsAppPlatform();
  //   await canLaunch(whatsappUrl)
  //       ? _showDialogWhatsAppPunya(context) //launch(whatsappUrl)
  //       : _showDialogWhatsAppTidakPunya(context);
  //   //: _launchDownloadWahtsApp();
  // }

  // _getUrlDownloadWhatsApp() {
  //   if (Platform.isIOS) {
  //     return 'https://apps.apple.com/id/app/whatsapp-messenger/id310633997';
  //   } else if (Platform.isAndroid) {
  //     return 'https://play.google.com/store/apps/details?id=com.whatsapp';
  //   }
  // }

  // _launchDownloadWahtsApp() async {
  //   var whatsAppDownloadUrl = _getUrlDownloadWhatsApp();
  //   await canLaunch(whatsAppDownloadUrl)
  //       ? launch(whatsAppDownloadUrl)
  //       : print('null');
  // }

  // //JIKA TIDAK PUNYA WHATSAPP
  // _showDialogWhatsAppTidakPunya(context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return Dialog(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(20.0)), //this right here
  //         child: Container(
  //           height: 400,
  //           child: Padding(
  //             padding: EdgeInsets.all(10.0),
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 SizedBox(height: 20),
  //                 // Container(
  //                 //   height: 70,
  //                 //   child: Image.asset("assets/images/icons/whatsapp-icon.png"),
  //                 // ),
  //                 Icon(
  //                   Icons.message,
  //                   size: 70,
  //                   color: Colors.green,
  //                 ),
  //                 SizedBox(height: 20),
  //                 Text(
  //                   //"Hai, untuk melakukan pembayaran pembelian produk, harap menghubungi Admin kami terlebih dahulu melalui WhatsApp.\nBelum punya WhatsApp? Download WhatsApp dulu ya! ",
  //                   "Hai, untuk melakukan pembayaran pembelian produk, harap menghubungi kami terlebih dahulu dengan cara menekan tombol 'Chat Dulu Yuk'",
  //                   style: TextStyle(fontSize: 15),
  //                   textAlign: TextAlign.center,
  //                 ),
  //                 Spacer(),
  //                 Column(
  //                   children: <Widget>[
  //                     Divider(
  //                       color: Colors.grey,
  //                     ),
  //                     FlatButton(
  //                       child: Text(
  //                         'Chat Dulu Yuk',
  //                         style: TextStyle(
  //                           color: Colors.blueAccent,
  //                           fontWeight: FontWeight.bold,
  //                           fontSize: 17,
  //                         ),
  //                       ),
  //                       onPressed: () {
  //                         //_launchDownloadWahtsApp();
  //                         launch(_getMessagePlatform());
  //                       },
  //                     ),
  //                     //Spacer(),
  //                     Divider(
  //                       color: Colors.grey,
  //                     ),
  //                     FlatButton(
  //                       child: Text(
  //                         'Lanjut Belanja',
  //                         style: TextStyle(color: Colors.grey),
  //                       ),
  //                       onPressed: () {
  //                         Navigator.of(context).pop();
  //                       },
  //                     ),
  //                     //Spacer(),
  //                   ],
  //                 )
  //               ],
  //             ),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }

  // //JIKA PUNYA WHATSAPP
  // _showDialogWhatsAppPunya(context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return Dialog(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(20.0)), //this right here
  //         child: Container(
  //           height: 400,
  //           child: Padding(
  //             padding: EdgeInsets.all(10.0),
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               children: [
  //                 SizedBox(height: 20),
  //                 Container(
  //                   height: 70,
  //                   child: Image.asset("assets/images/icons/whatsapp-icon.png"),
  //                 ),
  //                 SizedBox(height: 20),
  //                 Text(
  //                   "Hai, untuk melakukan pembayaran pembelian produk, harap menghubungi kami terlebih dahulu dengan cara menekan tombol 'Chat Dulu Yuk'",
  //                   style: TextStyle(fontSize: 15),
  //                   textAlign: TextAlign.center,
  //                 ),
  //                 Spacer(),
  //                 Column(
  //                   children: <Widget>[
  //                     Divider(
  //                       color: Colors.grey,
  //                     ),
  //                     FlatButton(
  //                       child: Text(
  //                         'Chat Dulu Yuk',
  //                         style: TextStyle(
  //                           color: Colors.blueAccent,
  //                           fontWeight: FontWeight.bold,
  //                           fontSize: 17,
  //                         ),
  //                       ),
  //                       onPressed: () {
  //                         launch(_getMessageWhatsAppPlatform());
  //                         //launch(_getMessagePlatform());
  //                       },
  //                     ),
  //                     //Spacer(),
  //                     Divider(
  //                       color: Colors.grey,
  //                     ),
  //                     FlatButton(
  //                       child: Text(
  //                         'Lanjut Belanja',
  //                         style: TextStyle(color: Colors.grey),
  //                       ),
  //                       onPressed: () {
  //                         Navigator.of(context).pop();
  //                       },
  //                     ),
  //                     //Spacer(),
  //                   ],
  //                 )
  //               ],
  //             ),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }

  @override
  void initState() {
    _getPrefKeyLog().then((k) {
      _decryptKey(k);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // BlocCartList bloccategorylist = BlocProvider.of<BlocCartList>(context);
    // bloccategorylist.dispatch(_decryptKeyReturn);
    BlocMGCartList blocmgcartlist = BlocProvider.of<BlocMGCartList>(context);
    blocmgcartlist.dispatch(_decryptKeyReturn);

    return Scaffold(
      body:
          // (_statusLogin == false)
          //     ? Container(
          //         height: MediaQuery.of(context).size.height / 1,
          //         child: Center(
          //           child: Column(
          //             mainAxisSize: MainAxisSize.max,
          //             mainAxisAlignment: MainAxisAlignment.center,
          //             children: <Widget>[
          //               Icon(
          //                 Icons.shopping_cart,
          //                 size: 100,
          //                 color: Colors.blue[300],
          //               ),
          //               SizedBox(
          //                 height: 30,
          //               ),
          //               Text(
          //                 "Lho, Keranjangnya Kok Kosong?",
          //                 style:
          //                     TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          //               ),
          //               SizedBox(
          //                 height: 10,
          //               ),
          //               Text(
          //                 "Ayo mulai belanja sekarang!",
          //                 style: TextStyle(fontSize: 18),
          //               ),
          //               SizedBox(
          //                 height: 10,
          //               ),
          //               Text(
          //                 "Habis belanja dapat Poin Arafah",
          //                 style: TextStyle(fontSize: 18),
          //               ),
          //             ],
          //           ),
          //         ),
          //       )
          //     :
          Stack(
        children: <Widget>[
          Container(
            //margin: EdgeInsets.only(bottom: 100),
            color: Colors.grey[200],
            height: MediaQuery.of(context).size.height / 1,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  BlocBuilder<BlocMGCartList, List<MGCartList>>(
                    builder: (context, itemlist) => (itemlist
                            is List<UninitializedMGCartList>)
                        ? Container(
                            height: MediaQuery.of(context).size.height / 1,
                            child: Center(
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                                backgroundColor: Colors.orangeAccent,
                              ),
                            ),
                          )
                        : (itemlist.length == 0)
                            ? Container(
                                height: MediaQuery.of(context).size.height / 1,
                                child: Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.shopping_cart,
                                        size: 100,
                                        color: Colors.blue[300],
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Text(
                                        "Lho, Keranjangnya Kok Kosong?",
                                        style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "MadeTommySoft",
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        "Ayo mulai belanja sekarang!",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontFamily: "MadeTommySoft",
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      // Text(
                                      //   "Habis belanja dapat Poin Arafah",
                                      //   style: TextStyle(fontSize: 18, fontFamily: "MadeTommySoft",),
                                      // ),
                                    ],
                                  ),
                                ),
                              )
                            : ListView.builder(
                                itemCount: itemlist.length,
                                physics: ClampingScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ComponentMGCart(itemlist[index]),
                              ),
                  ),
                  SizedBox(height: 60),
                ],
              ),
            ),
          ),

          //TOMBOL BAYAR
          BlocBuilder<BlocMGCartList, List<MGCartList>>(
            builder: (context, itemlist) =>
                (itemlist is List<UninitializedMGCartList>)
                    ? Container()
                    : (itemlist.length == 0)
                        ? Container()
                        : Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                SizedBox(),
                                Container(
                                  height: 70,
                                  padding: EdgeInsets.all(10),
                                  width: MediaQuery.of(context).size.width / 1,
                                  child: RaisedButton(
                                    onPressed: () {
                                      //_launchWhatsApp(context);
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                PageCartConfirm()),
                                      );
                                    },
                                    color: Colors.orangeAccent,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          "Lakukan Pembayaran",
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.white,
                                            fontFamily: "MadeTommySoft",
                                          ),
                                        ),
                                        Icon(
                                          Icons.exit_to_app,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(30),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
          ),
        ],
      ),
    );
  }
}
