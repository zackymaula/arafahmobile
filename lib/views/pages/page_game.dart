import 'package:flutter/material.dart';

class PageGame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.blueAccent),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width / 1,
        height: MediaQuery.of(context).size.height / 1,
        color: Colors.blue,
        child: Image(image: AssetImage("assets/images/animations/spinner.gif"),),
      ),
    );
  }
}