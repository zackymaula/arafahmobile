import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

class PageCurrency extends StatefulWidget {
  final bool popRoute;

  PageCurrency({this.popRoute = false});

  @override
  _PageCurrencyState createState() => _PageCurrencyState();
}

class _PageCurrencyState extends State<PageCurrency> {
  bool switchHK = false;
  bool switchTW = false;
  bool switchSG = false;
  bool switchMY = false;
  //var _statusLogin = false;
  //String _currency;

  // //CHECK KEY AUTH SHARE PREF > JIKA KOSONG LOGIN SET FALSE
  // _getPrefKeyLog() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   return pref.getString(Config().prefkeylog) ?? "";
  // }

  // //DECRYPT KEY > JIKA GAGAL STATUS LOGIN SET FALSE
  // _decryptKey(String keylog) {
  //   try {
  //     final key = encrypt.Key.fromUtf8(Config().key);
  //     final iv = encrypt.IV.fromLength(16);
  //     final encrypter = encrypt.Encrypter(encrypt.AES(key));

  //     final convertkeylog = encrypt.Encrypted.fromBase64(keylog);
  //     encrypter.decrypt(convertkeylog, iv: iv);
  //     _statusLogin = true;

  //     setState(() {});
  //   } catch (e) {
  //     //JIKA DECRYPT GAGAL > LOGIN SET FALSE
  //     _statusLogin = false;
  //     setState(() {});
  //   }
  // }

  //GET CURRENCY
  _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  //SET CURRENCY
  void savePrefData(String currency) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefcurrency, currency);
    });
  }

  void switchToggle(String country) {
    switch (country) {
      case 'HKD':
        switchHK = true;
        switchTW = false;
        switchSG = false;
        switchMY = false;
        savePrefData(country);
        setState(() {});
        break;
      case 'NTD':
        switchHK = false;
        switchTW = true;
        switchSG = false;
        switchMY = false;
        savePrefData(country);
        setState(() {});
        break;
      case 'SGD':
        switchHK = false;
        switchTW = false;
        switchSG = true;
        switchMY = false;
        savePrefData(country);
        setState(() {});
        break;
      case 'MYR':
        switchHK = false;
        switchTW = false;
        switchSG = false;
        switchMY = true;
        savePrefData(country);
        setState(() {});
        break;
      default:
    }
  }

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      (i != '') ? switchToggle(i) : switchToggle('HKD');
      setState(() {});
    });

    // _getPrefKeyLog().then((k) {
    //   (k == '') ? _statusLogin = false : _decryptKey(k);
    //   setState(() {});
    // });

    //_getPrefLocation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          //BACKGROUND
          Container(
            height: MediaQuery.of(context).size.height / 1,
            color: Colors.blue[800],
            // decoration: BoxDecoration(
            //   gradient: LinearGradient(
            //     colors: [Colors.orange, Colors.orange],
            //     begin: FractionalOffset.topCenter,
            //   ),
            // ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //PILIH NEGARA
              Container(
                width: MediaQuery.of(context).size.width / 1,
                margin: EdgeInsets.all(30),
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      blurRadius: 1,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Text(
                      'Pilih Negara / Mata Uang',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/images/flags/flag_hongkong.png",
                          fit: BoxFit.cover,
                          width: 70,
                        ),
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Hongkong",
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            Text(
                              "HKD",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Switch(
                          value: switchHK,
                          onChanged: (newValue) {
                            switchToggle('HKD');
                          },
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/images/flags/flag_taiwan.png",
                          fit: BoxFit.cover,
                          width: 70,
                        ),
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Taiwan",
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            Text(
                              "NTD",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Switch(
                          value: switchTW,
                          onChanged: (newValue) {
                            switchToggle('NTD');
                          },
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/images/flags/flag_singapore.png",
                          fit: BoxFit.cover,
                          width: 70,
                        ),
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Singapore",
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            Text(
                              "SGD",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Switch(
                          value: switchSG,
                          onChanged: (newValue) {
                            switchToggle('SGD');
                          },
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/images/flags/flag_malaysia.png",
                          fit: BoxFit.cover,
                          width: 70,
                        ),
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Malaysia",
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            Text(
                              "MYR",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Switch(
                          value: switchMY,
                          onChanged: (newValue) {
                            switchToggle('MYR');
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),

              //BUTTON PILIH
              Container(
                padding: EdgeInsets.only(left: 30, right: 30),
                width: MediaQuery.of(context).size.width / 1,
                height: 50,
                //color: Colors.blue,
                child: RaisedButton(
                  color: Colors.orangeAccent,
                  child: Text(
                    "Pilih",
                    style: TextStyle(
                      fontSize: 23,
                      color: Colors.white,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  onPressed: () {
                    (widget.popRoute == true)
                        ? Navigator.pop(context)
                        : Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (BuildContext context) => PageMain(),
                            ),
                            ModalRoute.withName('/'),
                          );
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
