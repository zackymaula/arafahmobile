import 'dart:io';

import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PageCartConfirm extends StatelessWidget {
  _getMessageWhatsAppPlatform() {
    var phone = '85252869420'; //NOMOR ARAFAH
    var text =
        'Hi ARAFAH, %0A%0ASaya sudah memasukkan produk ke keranjang My Arafah. Mohon dibantu bagaimana cara pembayarannya. %0A%0ATerima kasih';
    if (Platform.isIOS) {
      return 'whatsapp://send?phone=$phone';
    } else if (Platform.isAndroid) {
      return 'whatsapp://send?phone=$phone&text=$text';
    }
    return null;
  }

  _launchWhatsApp(context) async {
    var whatsappUrl = _getMessageWhatsAppPlatform();
    await canLaunch(whatsappUrl)
        ? launch(whatsappUrl)
        : _showDialogWhatsApp(context);
  }

  _getUrlDownloadWhatsApp() {
    if (Platform.isIOS) {
      return 'https://apps.apple.com/id/app/whatsapp-messenger/id310633997';
    } else if (Platform.isAndroid) {
      return 'https://play.google.com/store/apps/details?id=com.whatsapp';
    }
  }

  _launchDownloadWahtsApp() async {
    var whatsAppDownloadUrl = _getUrlDownloadWhatsApp();
    await canLaunch(whatsAppDownloadUrl)
        ? launch(whatsAppDownloadUrl)
        : print('null');
  }

  _showDialogWhatsApp(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 400,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Container(
                    height: 70,
                    child: Image.asset("assets/images/icons/whatsapp-icon.png"),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Belum punya WhatsApp? Download WhatsApp dulu ya!',
                    style: TextStyle(
                      fontSize: 20,
                      fontFamily: "MadeTommySoft",
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  Column(
                    children: <Widget>[
                      Divider(
                        color: Colors.grey,
                      ),
                      FlatButton(
                        child: Text(
                          'Download WhatsApp',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          _launchDownloadWahtsApp();
                        },
                      ),
                      //Spacer(),
                      Divider(
                        color: Colors.grey,
                      ),
                      FlatButton(
                        child: Text(
                          'Lanjut Belanja',
                          style: TextStyle(
                            color: Colors.grey,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      //Spacer(),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics-white.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.blue[900]),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.check_circle,
              size: 70,
              color: Colors.green,
            ),
            SizedBox(height: 20),
            Text(
              "Terima Kasih!",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            Text(
              "Sudah memesan produk kami",
              style: TextStyle(
                fontSize: 20,
                fontFamily: "MadeTommySoft",
              ),
              textAlign: TextAlign.center,
            ),
            // SizedBox(height: 20),
            // Text(
            //   "Pesanan sedang kami proses, \nsilahkan tunggu 1x24 jam, \nkami akan segera menghubungi Anda",
            //   style: TextStyle(
            //     fontSize: 15,
            //     fontFamily: "MadeTommySoft",
            //   ),
            //   textAlign: TextAlign.center,
            // ),
            SizedBox(height: 70),
            Container(
              height: 70,
              child: RaisedButton(
                color: Colors.green,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      width: 50,
                      child:
                          Image.asset("assets/images/icons/whatsapp-icon.png"),
                    ),
                    SizedBox(width: 10),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Hubungi Kami",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: Colors.white,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        Text(
                          "via WhatsApp",
                          style: TextStyle(
                            //fontWeight: FontWeight.bold,
                            fontSize: 23,
                            color: Colors.white, fontFamily: "MadeTommySoft",
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                onPressed: () {
                  _launchWhatsApp(context);
                },
              ),
            ),
            SizedBox(height: 20),
            Container(
              child: RaisedButton(
                color: Colors.grey,
                child: Text(
                  "Kembali ke Beranda",
                  style: TextStyle(
                    //fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white, fontFamily: "MadeTommySoft",
                  ),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30))),
                onPressed: () {
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (BuildContext context) => PageMain(),
                    ),
                    ModalRoute.withName('/'),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
