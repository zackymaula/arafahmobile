import 'package:arafahmobile/blocs/b_paket.dart';
import 'package:arafahmobile/models/m_paket.dart';
import 'package:arafahmobile/views/components/component_paket.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PagePaket extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC PAKET
        BlocProvider<BlocPaketList>(
          builder: (context) => BlocPaketList(),
        ),
      ],
      child: PagePaketView(),
    );
  }
}

class PagePaketView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocPaketList blocpaketlist = BlocProvider.of<BlocPaketList>(context);
    blocpaketlist.dispatch(1);

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics-white.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        child: BlocBuilder<BlocPaketList, List<PaketList>>(
          builder: (context, itemlist) =>
              (itemlist is List<UninitializedPaketList>)
                  ? Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.orangeAccent,
                      ),
                    )
                  : ListView.builder(
                      itemCount: itemlist.length,
                      itemBuilder: (context, index) =>
                          ComponentPaket(itemlist[index]),
                    ),
        ),
      ),
    );
  }
}
