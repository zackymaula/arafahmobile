import 'dart:io';

import 'package:arafahmobile/views/pages/page_alamat.dart';
import 'package:arafahmobile/views/pages/page_cooming_soon.dart';
import 'package:arafahmobile/views/pages/page_currency.dart';
import 'package:arafahmobile/views/pages/page_kontak_kami.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PageMenu extends StatelessWidget {
  PageMenu({Key key}) : super(key: key);

  _getMessageWhatsAppPlatform() {
    var phone = '85257039400'; //NOMOR ARAFAH
    var text =
        'Hi ARAFAH, %0A%0ASaya tertarik untuk berbelanja di ARAFAH. Mohon dibantu bagaimana cara belanja di ARAFAH. %0A%0ATerima kasih';
    if (Platform.isIOS) {
      return 'whatsapp://send?phone=$phone';
    } else if (Platform.isAndroid) {
      return 'whatsapp://send?phone=$phone&text=$text';
    }
    return null;
  }

  _launchWhatsApp(context) async {
    var whatsappUrl = _getMessageWhatsAppPlatform();
    await canLaunch(whatsappUrl)
        ? launch(whatsappUrl)
        : _showDialogWhatsApp(context);
  }

  _getUrlDownloadWhatsApp() {
    if (Platform.isIOS) {
      return 'https://apps.apple.com/id/app/whatsapp-messenger/id310633997';
    } else if (Platform.isAndroid) {
      return 'https://play.google.com/store/apps/details?id=com.whatsapp';
    }
  }

  _launchDownloadWahtsApp() async {
    var whatsAppDownloadUrl = _getUrlDownloadWhatsApp();
    await canLaunch(whatsAppDownloadUrl)
        ? launch(whatsAppDownloadUrl)
        : print('null');
  }

  _showDialogWhatsApp(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 400,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Container(
                    height: 70,
                    child: Image.asset("assets/images/icons/whatsapp-icon.png"),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Belum punya WhatsApp? Download WhatsApp dulu ya!',
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  Column(
                    children: <Widget>[
                      Divider(
                        color: Colors.grey,
                      ),
                      FlatButton(
                        child: Text(
                          'Download WhatsApp',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          _launchDownloadWahtsApp();
                        },
                      ),
                      //Spacer(),
                      Divider(
                        color: Colors.grey,
                      ),
                      FlatButton(
                        child: Text(
                          'Lanjut Belanja',
                          style: TextStyle(
                            color: Colors.grey,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      //Spacer(),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 50),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.blue[800], Colors.blue[800]],
            begin: FractionalOffset.topCenter,
          ),
        ),
        child: GridView.count(
          crossAxisCount: 3,
          childAspectRatio: 2.3 / 3,
          //scrollDirection: Axis.vertical,
          children: <Widget>[
            // Container(
            //   padding: EdgeInsets.all(5),
            //   child: Column(
            //     children: <Widget>[
            //       Container(
            //         width: 80,
            //         height: 80,
            //         //padding: EdgeInsets.all(10),
            //         //child: Image.asset("assets/images/icons/icon-arafah.png"),
            //         child: Icon(
            //           Icons.card_membership,
            //           size: 70,
            //           color: Colors.blue[800],
            //         ),
            //         decoration: BoxDecoration(
            //             color: Colors.white,
            //             borderRadius: BorderRadius.circular(20),
            //             boxShadow: [
            //               BoxShadow(
            //                   color: Colors.black.withOpacity(0.2),
            //                   blurRadius: 6,
            //                   offset: Offset(1, 1))
            //             ]),
            //       ),
            //       SizedBox(height: 10),
            //       Text("My Membership",
            //           style: TextStyle(fontWeight: FontWeight.bold))
            //     ],
            //   ),
            // ),
            // Container(
            //   padding: EdgeInsets.all(5),
            //   child: Column(
            //     children: <Widget>[
            //       GestureDetector(
            //         onTap: () => Navigator.of(context).push(
            //           MaterialPageRoute(
            //             builder: (BuildContext context) => PageCoomingSoon(),
            //           ),
            //         ),
            //         child: Container(
            //           width: 80,
            //           height: 80,
            //           //padding: EdgeInsets.all(10),
            //           //child: Image.asset("assets/images/icons/icon-arafah.png"),
            //           child: Icon(
            //             Icons.confirmation_number,
            //             size: 70,
            //             color: Colors.blue[800],
            //           ),
            //           decoration: BoxDecoration(
            //               color: Colors.white,
            //               borderRadius: BorderRadius.circular(20),
            //               boxShadow: [
            //                 BoxShadow(
            //                     color: Colors.black.withOpacity(0.2),
            //                     blurRadius: 6,
            //                     offset: Offset(1, 1))
            //               ]),
            //         ),
            //       ),
            //       SizedBox(height: 10),
            //       Text("My Voucher",
            //           style: TextStyle(fontWeight: FontWeight.bold))
            //     ],
            //   ),
            // ),
            // Container(
            //   padding: EdgeInsets.all(5),
            //   child: Column(
            //     children: <Widget>[
            //       GestureDetector(
            //         onTap: () => Navigator.of(context).push(
            //           MaterialPageRoute(
            //             builder: (BuildContext context) => PageCoomingSoon(),
            //           ),
            //         ),
            //         child: Container(
            //           width: 80,
            //           height: 80,
            //           //padding: EdgeInsets.all(10),
            //           //child: Image.asset("assets/images/icons/icon-arafah.png"),
            //           child: Icon(
            //             Icons.donut_small,
            //             size: 70,
            //             color: Colors.blue[800],
            //           ),
            //           decoration: BoxDecoration(
            //               color: Colors.white,
            //               borderRadius: BorderRadius.circular(20),
            //               boxShadow: [
            //                 BoxShadow(
            //                     color: Colors.black.withOpacity(0.2),
            //                     blurRadius: 6,
            //                     offset: Offset(1, 1))
            //               ]),
            //         ),
            //       ),
            //       SizedBox(height: 10),
            //       Text("My Point",
            //           style: TextStyle(fontWeight: FontWeight.bold))
            //     ],
            //   ),
            // ),
            Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            PageCurrency(popRoute: true),
                      ),
                    ),
                    child: Container(
                      width: 80,
                      height: 80,
                      //padding: EdgeInsets.all(10),
                      //child: Image.asset("assets/images/icons/icon-arafah.png"),
                      child: Icon(
                        Icons.monetization_on,
                        size: 70,
                        color: Colors.orange,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.2),
                                blurRadius: 6,
                                offset: Offset(1, 1))
                          ]),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Ganti Mata Uang",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontFamily: "MadeTommySoft",
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      _launchWhatsApp(context);
                      //FlutterOpenWhatsapp.sendSingleMessage("85257039400", "Hi, Saya tertarik untuk berbelanja di ARAFAH. Mohon dibantu bagaimana cara belanja di ARAFAH. Terima kasih");
                    },
                    child: Container(
                      padding: EdgeInsets.all(7),
                      width: 80,
                      height: 80,
                      child:
                          Image.asset("assets/images/icons/whatsapp-icon.png"),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.2),
                                blurRadius: 6,
                                offset: Offset(1, 1))
                          ]),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Chattingan Yuk",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => PageAlamat(),
                      ),
                    ),
                    child: Container(
                      width: 80,
                      height: 80,
                      //padding: EdgeInsets.all(10),
                      //child: Image.asset("assets/images/icons/icon-arafah.png"),
                      child: Icon(
                        Icons.store,
                        size: 70,
                        color: Colors.orange,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.2),
                                blurRadius: 6,
                                offset: Offset(1, 1))
                          ]),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Alamat Toko",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => PageKontakKami(),
                      ),
                    ),
                    child: Container(
                      width: 80,
                      height: 80,
                      //padding: EdgeInsets.all(10),
                      //child: Image.asset("assets/images/icons/icon-arafah.png"),
                      child: Icon(
                        Icons.phone,
                        size: 70,
                        color: Colors.orange,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.2),
                                blurRadius: 6,
                                offset: Offset(1, 1))
                          ]),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Kontak Kami",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
