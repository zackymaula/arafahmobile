import 'package:flutter/material.dart';

class PageBangunan extends StatelessWidget {
  final _bangunan = [
    'assets/images/bangunan/7.1-product-bahan-bangunan.jpg',
    'assets/images/bangunan/7.2-product-bahan-bangunan.jpg',
    'assets/images/bangunan/7.3-product-bahan-bangunan.jpg'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics-white.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _bangunan.length,
          itemBuilder: (context, index) => Container(
            height: 400,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: AssetImage(_bangunan[index]),
                fit: BoxFit.contain,
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(1),
                  blurRadius: 2,
                  offset: Offset(1, 1),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
