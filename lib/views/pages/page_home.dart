import 'dart:math';
import 'package:arafahmobile/blocs/b_category.dart';
import 'package:arafahmobile/blocs/b_mg_catalog_all.dart';
import 'package:arafahmobile/blocs/b_product_limit.dart';
import 'package:arafahmobile/blocs/b_promo.dart';
import 'package:arafahmobile/blocs/b_slide.dart';
import 'package:arafahmobile/models/m_category.dart';
import 'package:arafahmobile/models/m_mg_catalog_all.dart';
import 'package:arafahmobile/models/m_product_limit.dart';
import 'package:arafahmobile/models/m_promo.dart';
import 'package:arafahmobile/models/m_slide.dart';
import 'package:arafahmobile/views/components/component_category_home.dart';
import 'package:arafahmobile/views/components/component_mg_catalog_all.dart';
import 'package:arafahmobile/views/components/component_product_limit.dart';
import 'package:arafahmobile/views/components/component_promo.dart';
import 'package:arafahmobile/views/components/component_slide.dart';
import 'package:arafahmobile/views/pages/page_paket.dart';
import 'package:arafahmobile/views/pages/page_point.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class PageHome extends StatelessWidget {
  PageHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC SLIDE
        BlocProvider<BlocSlideList>(
          builder: (context) => BlocSlideList(),
        ),
        //BLOC CATEGORY
        BlocProvider<BlocCategoryList>(
          builder: (context) => BlocCategoryList(),
        ),
        // //BLOC PROMO
        // BlocProvider<BlocPromoList>(
        //   builder: (context) => BlocPromoList(),
        // ),
        // //BLOC PRODUCT LIMIT
        // BlocProvider<BlocProductListLimit>(
        //   builder: (context) => BlocProductListLimit(),
        // ),
        //BLOC MG CATALOG ALL
        BlocProvider<BlocMGCatalogAll>(
          builder: (context) => BlocMGCatalogAll(),
        ),
      ],
      child: PageHomeView(),
    );
  }
}

class PageHomeView extends StatelessWidget {
  //PageHomeView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocSlideList blocslidelist = BlocProvider.of<BlocSlideList>(context);
    BlocCategoryList bloccategorylist =
        BlocProvider.of<BlocCategoryList>(context);
    // BlocPromoList blocpromolist = BlocProvider.of<BlocPromoList>(context);
    // BlocProductListLimit blocproductlistlimit =
    //     BlocProvider.of<BlocProductListLimit>(context);
    BlocMGCatalogAll bloccatalogall =
        BlocProvider.of<BlocMGCatalogAll>(context);
    blocslidelist.dispatch(1);
    bloccategorylist.dispatch(1);
    // blocpromolist.dispatch(1);
    // blocproductlistlimit.dispatch(1);
    bloccatalogall.dispatch(1);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          //leading: Image.asset("assets/images/icons/icon-arafah.png", fit: BoxFit.cover, width: 10,),//Icon(Icons.home, color: Colors.blue[800]),
          title: Image.asset(
            "assets/images/icons/logo-arafahelectronics-white.png",
            fit: BoxFit.cover,
            height: 40,
          ),
          centerTitle: true,
          // actions: <Widget>[
          //   Container(padding: EdgeInsets.only(right: 15), child: Icon(Icons.notifications, color: Colors.blue[800])),
          // ],
        ),
        body: Stack(
          children: <Widget>[
            //BACKGROUND
            Container(
              height: MediaQuery.of(context).size.height / 1,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.grey[100], Colors.grey[100]],
                  begin: FractionalOffset.topCenter,
                ),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),

                  // //LIVE STREAM
                  // Row(
                  //   children: <Widget>[
                  //     Flexible(
                  //       flex: 1,
                  //       child: Container(
                  //         height: 300,
                  //         padding: EdgeInsets.all(10),
                  //         child: Stack(
                  //           children: <Widget>[
                  //             Container(
                  //               decoration: BoxDecoration(
                  //                 color: Colors.white,
                  //                 borderRadius: BorderRadius.circular(10),
                  //                 boxShadow: [
                  //                   BoxShadow(
                  //                     color: Colors.grey.withOpacity(1),
                  //                     blurRadius: 2,
                  //                     offset: Offset(1, 2),
                  //                   ),
                  //                 ],
                  //                 image: DecorationImage(
                  //                   fit: BoxFit.cover,
                  //                   image: AssetImage(
                  //                       "assets/images/others/example-live-1.jpg"),
                  //                 ),
                  //               ),
                  //             ),
                  //             Align(
                  //               alignment: Alignment.topRight,
                  //               child: Container(
                  //                 margin: EdgeInsets.all(10),
                  //                 padding: EdgeInsets.all(5),
                  //                 decoration: BoxDecoration(
                  //                   color: Colors.red,
                  //                   borderRadius: BorderRadius.circular(5),
                  //                   boxShadow: [
                  //                     BoxShadow(
                  //                       color: Colors.grey.withOpacity(1),
                  //                       blurRadius: 2,
                  //                       offset: Offset(1, 2),
                  //                     ),
                  //                   ],
                  //                 ),
                  //                 child: Text(
                  //                   "LIVE",
                  //                   style: TextStyle(
                  //                       color: Colors.white,
                  //                       fontSize: 10,
                  //                       fontWeight: FontWeight.bold),
                  //                 ),
                  //               ),
                  //             )
                  //           ],
                  //         ),
                  //       ),
                  //     ),
                  //     Flexible(
                  //       flex: 1,
                  //       child: Container(
                  //         height: 300,
                  //         padding: EdgeInsets.all(10),
                  //         child: Stack(
                  //           children: <Widget>[
                  //             Container(
                  //               decoration: BoxDecoration(
                  //                 color: Colors.white,
                  //                 borderRadius: BorderRadius.circular(10),
                  //                 boxShadow: [
                  //                   BoxShadow(
                  //                     color: Colors.grey.withOpacity(1),
                  //                     blurRadius: 2,
                  //                     offset: Offset(1, 2),
                  //                   ),
                  //                 ],
                  //                 image: DecorationImage(
                  //                   fit: BoxFit.cover,
                  //                   image: AssetImage(
                  //                       "assets/images/others/example-live-2.jpg"),
                  //                 ),
                  //               ),
                  //             ),
                  //             Align(
                  //               alignment: Alignment.topRight,
                  //               child: Container(
                  //                 margin: EdgeInsets.all(10),
                  //                 padding: EdgeInsets.all(5),
                  //                 decoration: BoxDecoration(
                  //                   color: Colors.red,
                  //                   borderRadius: BorderRadius.circular(5),
                  //                   boxShadow: [
                  //                     BoxShadow(
                  //                       color: Colors.grey.withOpacity(1),
                  //                       blurRadius: 2,
                  //                       offset: Offset(1, 2),
                  //                     ),
                  //                   ],
                  //                 ),
                  //                 child: Text(
                  //                   "LIVE",
                  //                   style: TextStyle(
                  //                       color: Colors.white,
                  //                       fontSize: 10,
                  //                       fontWeight: FontWeight.bold),
                  //                 ),
                  //               ),
                  //             )
                  //           ],
                  //         ),
                  //       ),
                  //     ),
                  //   ],
                  // ),

                  //SLIDE
                  BlocBuilder<BlocSlideList, List<SlideList>>(
                    builder: (context, slidelist) =>
                        (slidelist is List<UninitializedSlideList>)
                            ? Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  backgroundColor: Colors.orangeAccent,
                                ),
                              )
                            : CarouselSlider(
                                aspectRatio: 10 / 8,
                                enableInfiniteScroll: true,
                                reverse: false,
                                autoPlay: true,
                                autoPlayInterval: Duration(seconds: 3),
                                autoPlayAnimationDuration:
                                    Duration(milliseconds: 1000),
                                autoPlayCurve: Curves.fastOutSlowIn,
                                pauseAutoPlayOnTouch: Duration(seconds: 6),
                                enlargeCenterPage: true,
                                scrollDirection: Axis.horizontal,
                                items: slidelist.map(
                                  (i) {
                                    return Builder(
                                      builder: (BuildContext context) {
                                        return ComponentSlide(i);
                                      },
                                    );
                                  },
                                ).toList(),
                              ),
                  ),

                  //MY POINT
                  // Container(
                  //   width: MediaQuery.of(context).size.width / 1,
                  //   margin: EdgeInsets.fromLTRB(20, 25, 20, 5),
                  //   padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                  //   decoration: BoxDecoration(
                  //     color: Colors.white,
                  //     borderRadius: BorderRadius.all(
                  //       Radius.circular(30),
                  //     ),
                  //     boxShadow: [
                  //       BoxShadow(
                  //         color: Colors.black.withOpacity(0.3),
                  //         blurRadius: 6,
                  //         offset: Offset(1, 1),
                  //       ),
                  //     ],
                  //   ),
                  //   child: Column(
                  //     children: <Widget>[
                  //       Row(
                  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //         children: <Widget>[
                  //           Container(
                  //             height: 35,
                  //             child: Image.asset(
                  //                 "assets/images/icons/icon-arafah.png"),
                  //           ),
                  //           Text(
                  //             "0 Poin",
                  //             style: TextStyle(
                  //                 fontSize: 19, fontWeight: FontWeight.bold),
                  //           ),
                  //           RaisedButton(
                  //             onPressed: () {
                  //               Navigator.of(context).push(
                  //                 MaterialPageRoute(
                  //                   builder: (BuildContext context) =>
                  //                       PagePoint(),
                  //                 ),
                  //               );
                  //             },
                  //             child: Row(
                  //               children: <Widget>[
                  //                 Text(
                  //                   "Voucher",
                  //                   style: TextStyle(
                  //                       color: Colors.white,
                  //                       fontWeight: FontWeight.bold,
                  //                       fontSize: 15),
                  //                 ),
                  //                 SizedBox(width: 5),
                  //                 Icon(
                  //                   Icons.confirmation_number,
                  //                   color: Colors.white,
                  //                 )
                  //               ],
                  //             ),
                  //             color: Colors.orangeAccent,
                  //             shape: RoundedRectangleBorder(
                  //               borderRadius: BorderRadius.all(
                  //                 Radius.circular(5),
                  //               ),
                  //             ),
                  //           ),
                  //         ],
                  //       ),
                  //       Divider(
                  //         color: Colors.grey,
                  //       ),
                  //       Text(
                  //         "Ayo mainkan game untuk dapatkan hadiah !!!",
                  //         style: TextStyle(
                  //             fontWeight: FontWeight.bold, fontSize: 15),
                  //       ),
                  //       GestureDetector(
                  //         onTap: () => Navigator.of(context).push(
                  //           MaterialPageRoute(
                  //             builder: (BuildContext context) => PagePoint(),
                  //           ),
                  //         ),
                  //         child: Container(
                  //           height: 50,
                  //           margin: EdgeInsets.only(top: 10, bottom: 10),
                  //           decoration: BoxDecoration(
                  //             gradient: LinearGradient(
                  //               colors: [
                  //                 Colors.blue[900],
                  //                 Colors.blue[200],
                  //               ],
                  //             ),
                  //             borderRadius: BorderRadius.circular(30),
                  //             boxShadow: [
                  //               BoxShadow(
                  //                 color: Colors.black.withOpacity(0.5),
                  //                 blurRadius: 2,
                  //                 offset: Offset(3, 3),
                  //               ),
                  //             ],
                  //           ),
                  //           child: Row(
                  //             mainAxisAlignment: MainAxisAlignment.center,
                  //             children: <Widget>[
                  //               Text(
                  //                 "Play Game",
                  //                 style: TextStyle(
                  //                     fontSize: 30,
                  //                     color: Colors.white,
                  //                     fontWeight: FontWeight.bold),
                  //               ),
                  //               SizedBox(width: 10),
                  //               Icon(
                  //                 Icons.extension,
                  //                 color: Colors.white,
                  //                 size: 30,
                  //               )
                  //             ],
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  SizedBox(height: 10),
                  //PAKET ARAFAH
                  Container(
                    //margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: GestureDetector(
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => PagePaket(),
                        ),
                      ),
                      child: Container(
                        height: 50,
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(1),
                              blurRadius: 2,
                              offset: Offset(1, 2),
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 35,
                              child: Image.asset(
                                  "assets/images/icons/icon-arafah.png"),
                            ),
                            SizedBox(width: 10),
                            Text(
                              "Paket Seru dari ARAFAH",
                              style: TextStyle(
                                fontSize: 22,
                                //color: Colors,
                                fontWeight: FontWeight.bold,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

                  // SizedBox(height: 10),
                  // Container(
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     children: <Widget>[
                  //       Text(
                  //         "Kategori-Kategori Arafah",
                  //         style: TextStyle(
                  //             fontWeight: FontWeight.bold, fontSize: 15),
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  SizedBox(height: 10),
                  //KATEGORI
                  Container(
                    //margin: EdgeInsets.only(top: 5),
                    //padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width / 1,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 6,
                          child:
                              BlocBuilder<BlocCategoryList, List<CategoryList>>(
                            builder: (context, itemlist) => (itemlist
                                    is List<UninitializedCategoryList>)
                                ? Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                      backgroundColor: Colors.orangeAccent,
                                    ),
                                  )
                                : ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: itemlist.length,
                                    itemBuilder: (context, index) =>
                                        ComponentCategoryHome(itemlist[index]),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // SizedBox(height: 10),
                  // //PROMO BULAN
                  // Container(
                  //   width: MediaQuery.of(context).size.width / 1,
                  //   child: Column(
                  //     children: <Widget>[
                  //       Container(
                  //         margin: EdgeInsets.all(5),
                  //         child: Row(
                  //           mainAxisAlignment: MainAxisAlignment.center,
                  //           children: <Widget>[
                  //             Text(
                  //               "PROMO " +
                  //                   DateFormat('MMMM')
                  //                       .format(DateTime.now())
                  //                       .toUpperCase(),
                  //               style: TextStyle(
                  //                   fontWeight: FontWeight.bold, fontSize: 15),
                  //             ),
                  //             //Text("Lihat selengkapnya >")
                  //           ],
                  //         ),
                  //       ),
                  //       SizedBox(
                  //         height: 250,
                  //         child: BlocBuilder<BlocPromoList, List<PromoList>>(
                  //           builder: (context, itemlist) =>
                  //               (itemlist is List<UninitializedPromoList>)
                  //                   ? Center(
                  //                       child: CircularProgressIndicator(),
                  //                     )
                  //                   : ListView.builder(
                  //                       physics: ClampingScrollPhysics(),
                  //                       shrinkWrap: true,
                  //                       scrollDirection: Axis.horizontal,
                  //                       itemCount: itemlist.length,
                  //                       itemBuilder: (context, index) =>
                  //                           ComponentPromo(itemlist[index]),
                  //                     ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  // SizedBox(height: 10),
                  // //Produk-Produk
                  // Container(
                  //   width: MediaQuery.of(context).size.width / 1,
                  //   child: Column(
                  //     children: <Widget>[
                  //       Container(
                  //         margin: EdgeInsets.all(5),
                  //         child: Row(
                  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //           children: <Widget>[
                  //             Text(
                  //               "Produk-Produk Arafah",
                  //               style: TextStyle(fontWeight: FontWeight.bold),
                  //             ),
                  //             Text("Lihat selengkapnya >")
                  //           ],
                  //         ),
                  //       ),
                  //       SizedBox(
                  //         height: 220,
                  //         child: BlocBuilder<BlocProductListLimit,
                  //             List<ProductListLimit>>(
                  //           builder: (context, itemlist) => (itemlist
                  //                   is List<UninitializedProductListLimit>)
                  //               ? Center(
                  //                   child: CircularProgressIndicator(),
                  //                 )
                  //               : ListView.builder(
                  //                   physics: ClampingScrollPhysics(),
                  //                   shrinkWrap: true,
                  //                   scrollDirection: Axis.horizontal,
                  //                   itemCount: itemlist.length,
                  //                   itemBuilder: (context, index) =>
                  //                       ComponentProductLimit(itemlist[index]),
                  //                 ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  SizedBox(height: 10),
                  //PRODUCT-PRODUCT FROM MAGENTO
                  Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    width: MediaQuery.of(context).size.width / 1,
                    //color: Colors.yellow[200],
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          child:
                              BlocBuilder<BlocMGCatalogAll, List<MGCatalogAll>>(
                            builder: (context, itemlist) => (itemlist
                                    is List<UninitializedMGCatalogAll>)
                                ? Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                      backgroundColor: Colors.orangeAccent,
                                    ),
                                  )
                                : GridView.builder(
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: 2.2 / 3,
                                    ),
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: itemlist.length,
                                    itemBuilder: (context, index) =>
                                        ComponentMGCatalogAll(itemlist[index]),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
