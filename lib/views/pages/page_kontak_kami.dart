import 'package:flutter/material.dart';

class PageKontakKami extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics-white.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  width: 30,
                  height: 30,
                  //padding: EdgeInsets.all(10),
                  child: Image.asset("assets/images/icons/whatsapp-icon.png"),
                ),
                SizedBox(width: 10),
                Text(
                  '(+852) 5286-9420',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Container(
                  width: 30,
                  height: 30,
                  //padding: EdgeInsets.all(10),
                  child: Image.asset("assets/images/icons/facebook-icon.png"),
                ),
                SizedBox(width: 10),
                Text(
                  'halloarafah',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Container(
                  width: 30,
                  height: 30,
                  //padding: EdgeInsets.all(10),
                  child: Image.asset("assets/images/icons/instagram-icon.png"),
                ),
                SizedBox(width: 10),
                Text(
                  'halloarafah.id',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            // Row(
            //   children: <Widget>[
            //     Icon(
            //       Icons.phone,
            //       size: 30,
            //       color: Colors.black87,
            //     ),
            //     SizedBox(width: 10),
            //     Text(
            //       '(+852) 5730-9400',
            //       style: TextStyle(fontSize: 25),
            //     ),
            //   ],
            // ),
            // SizedBox(height: 10),
            Row(
              children: <Widget>[
                Icon(
                  Icons.phone,
                  size: 30,
                  color: Colors.black87,
                ),
                SizedBox(width: 10),
                Text(
                  '(+852) 5286-9420',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Icon(
                  Icons.phone,
                  size: 30,
                  color: Colors.black87,
                ),
                SizedBox(width: 10),
                Text(
                  '(+852) 2802-9318',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Icon(
                  Icons.phone,
                  size: 30,
                  color: Colors.black87,
                ),
                SizedBox(width: 10),
                Text(
                  '(+852) 2802-9328',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Icon(
                  Icons.email,
                  size: 30,
                  color: Colors.black87,
                ),
                SizedBox(width: 10),
                Text(
                  'info@arafah.hk',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Icon(
                  Icons.web,
                  size: 30,
                  color: Colors.black87,
                ),
                SizedBox(width: 10),
                Text(
                  'www.arafah.hk',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Icon(
                  Icons.web,
                  size: 30,
                  color: Colors.black87,
                ),
                SizedBox(width: 10),
                Text(
                  'www.arafah.tw',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
