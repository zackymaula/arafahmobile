import 'package:arafahmobile/blocs/b_auth.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_auth.dart';
import 'package:arafahmobile/views/pages/login/page_input_nomor.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageUserMain extends StatelessWidget {
  PageUserMain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC AUTH GET
        BlocProvider<BlocAuthenticationGet>(
          builder: (context) => BlocAuthenticationGet(),
        ),
      ],
      child: PageUserMainView(),
    );
  }
}

class PageUserMainView extends StatefulWidget {
  @override
  _PageUserMainViewState createState() => _PageUserMainViewState();
}

class _PageUserMainViewState extends State<PageUserMainView> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;

  FirebaseAuth _auth = FirebaseAuth.instance;
  var _decryptKeyReturn;
  var _memberkeylog;
  // var _memberNamaDepan;
  // var _memberNamaBelakang;
  // var _memberTanggalLahir;
  // var _memberFacebookName;
  // var _memberFacebookEmail;
  // var _memberFacebookPhoto;
  // var _memberLocationUrl;
  // var _memberLocationFull;

  //GET KEY-AUTH SHAREPREF
  Future<Null> _getPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      _memberkeylog = pref.getString(Config().prefmemberkeylog) ?? "";
      // _memberNamaDepan = pref.getString(Config().prefmembernamadepan) ?? "";
      // _memberNamaBelakang =
      //     pref.getString(Config().prefmembernamabelakang) ?? "";
      // _memberTanggalLahir = pref.getString(Config().prefmembertgllahir) ?? "";
      // _memberFacebookName =
      //     pref.getString(Config().prefmemberfacebookname) ?? "";
      // _memberFacebookEmail =
      //     pref.getString(Config().prefmemberfacebookemail) ?? "";
      // _memberFacebookPhoto =
      //     pref.getString(Config().prefmemberfacebookphoto) ?? "";
      // _memberLocationUrl = pref.getString(Config().prefmemberlocationurl) ?? "";
      // _memberLocationFull =
      //     pref.getString(Config().prefmemberlocationfull) ?? "";
      _decryptKeyReturn = _decryptKey(_memberkeylog);
    });
  }

  //DECRYPT KEY > JIKA GAGAL MOVE TO PAGE LOGIN
  _decryptKey(String keylog) {
    try {
      final key = encrypt.Key.fromUtf8(Config().key);
      final iv = encrypt.IV.fromLength(16);
      final encrypter = encrypt.Encrypter(encrypt.AES(key));

      final convertkeylog = encrypt.Encrypted.fromBase64(keylog);
      final decryptkeylog = encrypter.decrypt(convertkeylog, iv: iv);
      //_decryptKeyReturn = encrypter.decrypt(convertkeylog, iv: iv);
      return decryptkeylog;
    } catch (e) {
      //JIKA DECRYPT KEY-NYA GAGAL
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => PageLoginInputNomor()),
        (Route<dynamic> route) => false,
      );
      _savePrefDataLogout();
    }
  }

  //LOGOUT SET KEY AUTH SHAREPREF NULL
  Future<Null> _savePrefDataLogout() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberkeylog, '');
      pref.setString(Config().prefmembernomorhp, '');
      pref.setString(Config().prefmembernamadepan, '');
      pref.setString(Config().prefmembernamabelakang, '');
      pref.setString(Config().prefmembertgllahir, '');
      pref.setString(Config().prefmemberphotoidcard, '');
      pref.setString(Config().prefmemberfacebookname, '');
      pref.setString(Config().prefmemberfacebookemail, '');
      pref.setString(Config().prefmemberfacebookphone, '');
      pref.setString(Config().prefmemberfacebookphoto, '');
      pref.setString(Config().prefmemberlocationurl, '');
      pref.setString(Config().prefmemberlocationfull, '');
    });
  }

  @override
  void initState() {
    super.initState();
    //_getCurrentLocation();
    _getPref();
  }

  @override
  Widget build(BuildContext context) {
    BlocAuthenticationGet blocauthget =
        BlocProvider.of<BlocAuthenticationGet>(context);
    try {
      blocauthget.dispatch(_decryptKeyReturn);
    } catch (e) {
      print(e);
    }
    //blocauth.dispatch('5ibJiNXmP0QvCVvuLvGXbEYDBAD3+space++85298765432');

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.blueAccent),
      ),
      body: BlocBuilder<BlocAuthenticationGet, AuthenticationGet>(
        builder: (context, item) => (item is UninitializedAuthenticationGet)
            ? Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.orangeAccent,
                ), //JIKA TIDAK ADA DATA
              )
            : Container(
                color: Colors.grey[200],
                height: MediaQuery.of(context).size.height / 1,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      //NAME
                      Container(
                        //height: 140,
                        width: MediaQuery.of(context).size.width / 1,
                        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                        //color: Color(int.parse(_categoryColor[index])),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.blue[900],
                              Colors.blue[900],
                            ],
                          ),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(30),
                              bottomLeft: Radius.circular(30)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.5),
                              blurRadius: 5,
                              offset: Offset(1, 1),
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            SizedBox(height: 25),
                            Icon(
                              Icons.person_pin,
                              color: Colors.lightGreenAccent,
                              size: 40,
                            ),
                            SizedBox(height: 10),
                            Text(
                              'Hai, ' + item.membernamadepan,
                              style: TextStyle(
                                fontSize: 21,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "MadeTommySoft",
                              ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 10),
                            Divider(
                              color: Colors.white,
                            ),
                            // Text(
                            //   "0 Poin",
                            //   style:
                            //       TextStyle(fontSize: 20, color: Colors.white),
                            // ),
                            SizedBox(height: 25),
                          ],
                        ),
                      ),

                      SizedBox(height: 20),

                      //DATA
                      Container(
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              blurRadius: 6,
                              offset: Offset(1, 1),
                            ),
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            //NAMA
                            TextField(
                              controller: TextEditingController(
                                  text: item.membernamadepan +
                                      ' ' +
                                      item.membernamabelakang),
                              decoration: InputDecoration(
                                filled: true,
                                labelText: 'Nama Lengkap',
                                prefixIcon: Icon(Icons.account_circle,
                                    color: Colors.blueAccent),
                                labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MadeTommySoft",
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabled: false,
                              ),
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            SizedBox(height: 20),
                            //Nomor HP
                            TextField(
                              controller:
                                  TextEditingController(text: item.memberphone),
                              decoration: InputDecoration(
                                filled: true,
                                labelText: 'Nomor Handphone',
                                prefixIcon: Icon(Icons.phone_android,
                                    color: Colors.blueAccent),
                                labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MadeTommySoft",
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabled: false,
                              ),
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            SizedBox(height: 20),
                            //Tanggal Lahir
                            TextField(
                              controller: TextEditingController(
                                  text: item.membertgllahir),
                              decoration: InputDecoration(
                                filled: true,
                                labelText: 'Tanggal Lahir',
                                prefixIcon: Icon(Icons.date_range,
                                    color: Colors.blueAccent),
                                labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MadeTommySoft",
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabled: false,
                              ),
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            //SizedBox(height: 20),
                            // TextField(
                            //   controller: TextEditingController(
                            //       text: _memberLocationFull),
                            //   decoration: InputDecoration(
                            //     filled: true,
                            //     labelText: 'Alamat',
                            //     prefixIcon: Icon(Icons.location_on,
                            //         color: Colors.blueAccent),
                            //     labelStyle: TextStyle(
                            //       fontWeight: FontWeight.bold,
                            //       fontFamily: "MadeTommySoft",
                            //     ),
                            //     border: OutlineInputBorder(
                            //       borderRadius: BorderRadius.circular(10),
                            //     ),
                            //     enabled: false,
                            //   ),
                            //   style: TextStyle(
                            //     fontSize: 20,
                            //     fontFamily: "MadeTommySoft",
                            //   ),
                            //   keyboardType: TextInputType.multiline,
                            //   maxLines: 4,
                            // ),
                          ],
                        ),
                      ),

                      //DATA FACEBOOK
                      Container(
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              blurRadius: 6,
                              offset: Offset(1, 1),
                            ),
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: 60,
                                  height: 60,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/icons/facebook-icon.png'),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 20),
                                Icon(
                                  Icons.compare_arrows,
                                  color: Colors.green,
                                  size: 30,
                                ),
                                SizedBox(width: 20),
                                CachedNetworkImage(
                                  imageUrl: item.memberfacebookphoto,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    height: 60,
                                    width: 60,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(40),
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  placeholder: (context, url) => Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                      backgroundColor: Colors.orangeAccent,
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.verified_user,
                                  color: Colors.green,
                                  size: 20,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  'Sudah terkoneksi dengan Facebook',
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "MadeTommySoft",
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 40),
                            //NAMA
                            TextField(
                              controller: TextEditingController(
                                  text: item.memberfacebookname),
                              decoration: InputDecoration(
                                filled: true,
                                labelText: 'Nama Facebook',
                                prefixIcon: Icon(Icons.person,
                                    color: Colors.blueAccent),
                                labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MadeTommySoft",
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabled: false,
                              ),
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                            SizedBox(height: 20),
                            //Email
                            TextField(
                              controller: TextEditingController(
                                  text: item.memberfacebookemail),
                              decoration: InputDecoration(
                                filled: true,
                                labelText: 'Email Facebook',
                                prefixIcon:
                                    Icon(Icons.email, color: Colors.blueAccent),
                                labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MadeTommySoft",
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                enabled: false,
                              ),
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: "MadeTommySoft",
                              ),
                            ),
                          ],
                        ),
                      ),

                      SizedBox(height: 20),

                      // Container(
                      //   padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      //   width: MediaQuery.of(context).size.width / 1,
                      //   height: 40,
                      //   child: RaisedButton(
                      //     color: Colors.orangeAccent,
                      //     child: Row(
                      //       mainAxisAlignment: MainAxisAlignment.center,
                      //       children: <Widget>[
                      //         Text(
                      //           "Edit Nama",
                      //           style: TextStyle(
                      //               fontWeight: FontWeight.bold,
                      //               fontSize: 17,
                      //               color: Colors.white),
                      //         ),
                      //         SizedBox(width: 10),
                      //         Icon(
                      //           Icons.build,
                      //           color: Colors.white,
                      //         ),
                      //       ],
                      //     ),
                      //     shape: RoundedRectangleBorder(
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(30))),
                      //     onPressed: () {},
                      //   ),
                      // ),

                      // SizedBox(height: 20),

                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        width: MediaQuery.of(context).size.width / 1,
                        height: 40,
                        child: RaisedButton(
                          color: Colors.redAccent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Logout",
                                style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                  fontFamily: "MadeTommySoft",
                                ),
                              ),
                              SizedBox(width: 10),
                              Icon(
                                Icons.power_settings_new,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                          onPressed: () {
                            _logOutProcess(context);
                          },
                        ),
                      ),

                      SizedBox(height: 50),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  _logOutProcess(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 300,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Icon(
                    Icons.power_settings_new,
                    color: Colors.redAccent,
                    size: 70,
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Yakin ingin logout dari aplikasi?',
                    style: TextStyle(
                      fontSize: 25,
                      fontFamily: "MadeTommySoft",
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          'Logout',
                          style: TextStyle(
                            color: Colors.redAccent,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () async {
                          await _auth.signOut();
                          _savePrefDataLogout();
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => PageLoginInputNomor()),
                            (Route<dynamic> route) => false,
                          );
                        },
                      ),
                      Spacer(),
                      FlatButton(
                        child: Text(
                          'Tidak',
                          style: TextStyle(
                            color: Colors.blue[800],
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  //GET LOCATION LAT LONG
  _getCurrentLocation() async {
    //final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    try {
      await geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) {
        setState(() {
          _currentPosition = position;
        });

        _getAddressFromLatLng();
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }
  }

  //GET LOCATION FULL
  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.thoroughfare}, ${place.subThoroughfare}, ${place.subLocality}, ${place.locality}, ${place.subAdministrativeArea}, ${place.administrativeArea}, ${place.country}, ${place.postalCode}";

        //print('otp ' +_currentPosition.toString());
        //print('otp ' +_currentAddress);
        _savePrefDataLocation();
      });
    } catch (e) {
      print(e);
    }
  }

  //SET LOCATION TO PREF
  Future<Null> _savePrefDataLocation() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString(Config().prefmemberlocationurl,
          'https://www.google.co.id/maps/@${_currentPosition.latitude.toString()},${_currentPosition.longitude.toString()},20z');
      pref.setString(
          Config().prefmemberlocationfull, _currentAddress.toString());
    });
  }
}
