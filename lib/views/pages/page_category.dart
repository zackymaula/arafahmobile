import 'package:arafahmobile/blocs/b_category.dart';
import 'package:arafahmobile/models/m_category.dart';
import 'package:arafahmobile/views/components/component_category.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PageCategory extends StatelessWidget {
  PageCategory({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC CATEGORY
        BlocProvider<BlocCategoryList>(
          builder: (context) => BlocCategoryList(),
        ),
      ],
      child: PageCategoryView(),
    );
  }
}

class PageCategoryView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocCategoryList bloccategorylist =
        BlocProvider.of<BlocCategoryList>(context);
    bloccategorylist.dispatch(1);

    return Scaffold(
      body: Container(
        //margin: EdgeInsets.only(top: 10),
        child: BlocBuilder<BlocCategoryList, List<CategoryList>>(
          builder: (context, itemlist) =>
              (itemlist is List<UninitializedCategoryList>)
                  ? Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.orangeAccent,
                      ),
                    )
                  : ListView.builder(
                      itemCount: itemlist.length,
                      itemBuilder: (BuildContext context, int index) =>
                          ComponentCategory(itemlist[index]),
                    ),
        ),
      ),
    );
  }
}
