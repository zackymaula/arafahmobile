import 'package:arafahmobile/views/pages/page_cooming_soon.dart';
import 'package:arafahmobile/views/pages/page_game.dart';
import 'package:flutter/material.dart';

class PagePoint extends StatelessWidget {
  final _categoryLabel = [
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
  ];

  final _categoryImage = [
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
    'assets/images/others/coming-soon.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/icons/logo-arafahelectronics.png",
          fit: BoxFit.cover,
          height: 40,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.blueAccent),
      ),
      body: Container(
        color: Colors.grey[200],
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //POINT
              Container(
                //height: 140,
                width: MediaQuery.of(context).size.width / 1,
                //padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                //color: Color(int.parse(_categoryColor[index])),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blue[900],
                      Colors.blue[200],
                    ],
                  ),
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(200),
                      bottomLeft: Radius.circular(200)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(height: 30),
                    Text(
                      "My Point",
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    Text(
                      "(Coming Soon)",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontStyle: FontStyle.italic),
                    ),
                    // Text(
                    //   "0",
                    //   style: TextStyle(fontSize: 70, color: Colors.white),
                    // ),
                    Icon(
                      Icons.beenhere,
                      //Icons.bookmark,
                      color: Colors.white,
                      size: 100,
                    ),
                    SizedBox(height: 30),
                  ],
                ),
              ),

              //GAME
              Container(
                width: MediaQuery.of(context).size.width / 1,
                margin: EdgeInsets.fromLTRB(20, 25, 20, 5),
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      blurRadius: 6,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    // Text(
                    //   "DAPATKAN HADIAH !!!",
                    //   style:
                    //       TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                    // ),
                    GestureDetector(
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => PageCoomingSoon(),
                        ),
                      ),
                      child: Container(
                        height: 50,
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.blue[900],
                              Colors.blue[200],
                            ],
                          ),
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.5),
                              blurRadius: 2,
                              offset: Offset(3, 3),
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Play Game",
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 10),
                            Icon(
                              Icons.extension,
                              color: Colors.white,
                              size: 30,
                            )
                          ],
                        ),
                      ),
                    ),
                    Text(
                      "(Coming Soon)",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ),

              //VOUCHER
              Container(
                padding: EdgeInsets.only(top: 10),
                width: MediaQuery.of(context).size.width / 1,
                //color: Colors.yellow[200],
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 280.0,
                      child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: _categoryLabel.length,
                        itemBuilder: (BuildContext context, int index) => Stack(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.7),
                                    blurRadius: 5,
                                    offset: Offset(2, 2),
                                  ),
                                ],
                              ),
                              child: Center(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      height: 200,
                                      width: 200,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(20),
                                          topLeft: Radius.circular(20),
                                        ),
                                        image: DecorationImage(
                                          fit: BoxFit.contain,
                                          image:
                                              AssetImage(_categoryImage[index]),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    // Text(
                                    //   _categoryLabel[index] + ' Poin',
                                    //   style: TextStyle(
                                    //       fontWeight: FontWeight.bold),
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              //color: Colors.grey,
                              width: 220,
                              height: 280,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  RaisedButton(
                                    onPressed: () {},
                                    child: Text(
                                      "Dapatkan Voucher",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    color: Colors.orangeAccent,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }
}
