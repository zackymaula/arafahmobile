import 'package:arafahmobile/blocs/b_category_sub.dart';
import 'package:arafahmobile/blocs/b_mg_catalog_category.dart';
import 'package:arafahmobile/blocs/b_product_category.dart';
import 'package:arafahmobile/models/m_category_sub.dart';
import 'package:arafahmobile/models/m_mg_catalog_category.dart';
import 'package:arafahmobile/models/m_product_category.dart';
import 'package:arafahmobile/views/components/component_category_sub.dart';
import 'package:arafahmobile/views/components/component_mg_catalog_category.dart';
import 'package:arafahmobile/views/components/component_product_category.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PageCategoryProductList extends StatelessWidget {
  final int categoryId;
  final String categoryName;
  final String categoryImage;

  PageCategoryProductList(
      {this.categoryId, this.categoryName, this.categoryImage});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC CATEGORY SUB
        BlocProvider<BlocCategorySubList>(
          builder: (context) => BlocCategorySubList(),
        ),
        //BLOC PRODUCT CATEGORY
        // BlocProvider<BlocProductListCategory>(
        //   builder: (context) => BlocProductListCategory(),
        // ),
        //BLOC PRODUCT CATEGORY MAGENTO
        BlocProvider<BlocMGCatalogCategory>(
          builder: (context) => BlocMGCatalogCategory(),
        ),
      ],
      child: PageCategoryProductListView(
        categoryId: categoryId,
        categoryName: categoryName,
        categoryImage: categoryImage,
      ),
    );
  }
}

class PageCategoryProductListView extends StatelessWidget {
  final int categoryId;
  final String categoryName;
  final String categoryImage;

  PageCategoryProductListView(
      {this.categoryId, this.categoryName, this.categoryImage});

  @override
  Widget build(BuildContext context) {
    BlocCategorySubList bloccategorylist =
        BlocProvider.of<BlocCategorySubList>(context);
    bloccategorylist.dispatch(categoryId);
    // BlocProductListCategory blocproductcategory =
    //     BlocProvider.of<BlocProductListCategory>(context);
    //blocproductcategory.dispatch(categoryId);
    BlocMGCatalogCategory blocmgcatalogcategory =
        BlocProvider.of<BlocMGCatalogCategory>(context);
    blocmgcatalogcategory.dispatch(categoryName);

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text(
              categoryName,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
          ],
        ),
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        color: Colors.grey[200],
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //HEADER
              Container(
                height: 140,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                //color: Color(int.parse(_categoryColor[index])),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.blue[800],
                      Colors.blue[200],
                    ],
                  ),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(50)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(1),
                      blurRadius: 5,
                      offset: Offset(2, 2),
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      categoryName,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: "MadeTommySoft",
                      ),
                    ),
                    CachedNetworkImage(
                      imageUrl: categoryImage,
                      imageBuilder: (context, imageProvider) => Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          backgroundColor: Colors.orangeAccent,
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 20),

              //SUB KATEGORI
              Container(
                width: MediaQuery.of(context).size.width / 1,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 6,
                      child: BlocBuilder<BlocCategorySubList,
                          List<CategorySubList>>(
                        builder: (context, itemlist) =>
                            (itemlist is List<UninitializedCategorySubList>)
                                ? Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                      backgroundColor: Colors.orangeAccent,
                                    ),
                                  )
                                : ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: itemlist.length,
                                    itemBuilder: (context, index) =>
                                        ComponentCategorySub(itemlist[index]),
                                  ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 10),
              Divider(color: Colors.grey),
              SizedBox(height: 10),

              //PRODUCT-PRODUCT
              // Container(
              //   margin: EdgeInsets.only(left: 5, right: 5),
              //   width: MediaQuery.of(context).size.width / 1,
              //   child: Column(
              //     children: <Widget>[
              //       SizedBox(
              //         child: BlocBuilder<BlocProductListCategory,
              //             List<ProductListCategory>>(
              //           builder: (context, itemlist) => (itemlist
              //                   is List<UninitializedProductListCategory>)
              //               ? Center(
              //                   child: CircularProgressIndicator(),
              //                 )
              //               : GridView.builder(
              //                   gridDelegate:
              //                       SliverGridDelegateWithFixedCrossAxisCount(
              //                           crossAxisCount: 2,
              //                           childAspectRatio: 2.3 / 3),
              //                   physics: ClampingScrollPhysics(),
              //                   shrinkWrap: true,
              //                   itemCount: itemlist.length,
              //                   itemBuilder: (context, index) =>
              //                       ComponentProductCategory(itemlist[index]),
              //                 ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),

              //PRODUCT-PRODUCT FROM MAGENTO
              Container(
                margin: EdgeInsets.only(left: 5, right: 5),
                width: MediaQuery.of(context).size.width / 1,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      child: BlocBuilder<BlocMGCatalogCategory,
                          List<MGCatalogCategory>>(
                        builder: (context, itemlist) => (itemlist
                                is List<UninitializedMGCatalogCategory>)
                            ? Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  backgroundColor: Colors.orangeAccent,
                                ),
                              )
                            : GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        childAspectRatio: 2.2 / 3),
                                physics: ClampingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: itemlist.length,
                                itemBuilder: (context, index) =>
                                    ComponentMGCatalogCategory(itemlist[index]),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
