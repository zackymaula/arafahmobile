import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_mg_catalog_all.dart';
import 'package:arafahmobile/views/pages/page_mg_catalog_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentMGCatalogAll extends StatefulWidget {
  final MGCatalogAll catalogAll;
  ComponentMGCatalogAll(this.catalogAll);

  @override
  _ComponentMGProductListState createState() => _ComponentMGProductListState();
}

class _ComponentMGProductListState extends State<ComponentMGCatalogAll> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      var price = int.parse(widget.catalogAll.price);

      if (i == 'HKD') {
        _price = (price * 1).toString() + ' ' + i;
      } else if (i == 'NTD') {
        _price = (price * 4).toString() + ' ' + i;
      } else if (i == 'SGD') {
        _price = (price * 0.2).toString() + ' ' + i;
      } else if (i == 'MYR') {
        _price = (price * 0.6).toString() + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageMGCatalogDetail(
            id: widget.catalogAll.id,
            name: widget.catalogAll.name,
            sku: widget.catalogAll.sku,
            price: widget.catalogAll.price,
            description: widget.catalogAll.description,
            image: widget.catalogAll.image,
            categorynames: widget.catalogAll.categorynames,
            categorysubnames: widget.catalogAll.categorysubnames,
          ),
        ),
      ),
      child: Card(
        elevation: 2,
        child: Container(
          margin: EdgeInsets.all(3),
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: Config().mgpathimage + widget.catalogAll.image,
                    imageBuilder: (context, imageProvider) => Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      height: MediaQuery.of(context).size.height / 4.9,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.orangeAccent,
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Text(
                widget.catalogAll.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  fontFamily: "MadeTommySoft",
                ),
              ),
              SizedBox(height: 5),
              Text(
                _price,
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  fontFamily: "MadeTommySoft",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
