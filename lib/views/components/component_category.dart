import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_category.dart';
import 'package:arafahmobile/views/pages/page_bangunan.dart';
import 'package:arafahmobile/views/pages/page_category_product_list.dart';
import 'package:arafahmobile/views/pages/page_paket.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ComponentCategory extends StatelessWidget {
  final CategoryList categoryList;
  ComponentCategory(this.categoryList);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => (categoryList.idcategories == 1) //PAKET ARAFAH
          ? Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => PagePaket(),
              ),
            )
          : (categoryList.idcategories == 7) //BAHAN BANGUNAN
              ? Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => PageBangunan(),
                  ),
                )
              : Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => PageCategoryProductList(
                      categoryId: categoryList.idcategories,
                      categoryName: categoryList.name,
                      categoryImage: Config().ipmain +
                          categoryList.filepath +
                          categoryList.filename,
                    ),
                  ),
                ),
      child: Container(
        height: 140,
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        //color: Color(int.parse(_categoryColor[index])),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue[900],
              Colors.blue[200],
            ],
          ),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              blurRadius: 5,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              categoryList.name,
              style: TextStyle(
                fontSize: 25,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: "MadeTommySoft",
              ),
            ),
            CachedNetworkImage(
              imageUrl: Config().ipmain +
                  categoryList.filepath +
                  categoryList.filename,
              imageBuilder: (context, imageProvider) => Container(
                width: MediaQuery.of(context).size.width / 5,
                height: MediaQuery.of(context).size.height / 9,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: imageProvider, fit: BoxFit.contain),
                ),
              ),
              placeholder: (context, url) => Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.orangeAccent,
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ],
        ),
      ),
    );
  }
}
