import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_category_sub.dart';
import 'package:arafahmobile/views/pages/page_categorysub_product_list.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ComponentCategorySub extends StatelessWidget {
  final CategorySubList categorySubList;
  ComponentCategorySub(this.categorySubList);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageCategorySubProductList(
            categoryId: categorySubList.idcategories,
            categorySubId: categorySubList.idcategorysub,
            categoryName: categorySubList.categoryname,
            categorySubName: categorySubList.categorysubname,
            categoryImage: Config().ipmain +
                categorySubList.filepath +
                categorySubList.filename,
          ),
        ),
      ),
      child: Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(5),
          child: Center(
            child: Column(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: Config().ipmain +
                      categorySubList.filepath +
                      categorySubList.filename,
                  imageBuilder: (context, imageProvider) => Container(
                    width: MediaQuery.of(context).size.width / 5,
                    height: MediaQuery.of(context).size.height / 9,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      backgroundColor: Colors.orangeAccent,
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  categorySubList.categorysubname,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
