import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_product_limit.dart';
import 'package:arafahmobile/views/pages/page_product_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentProductLimit extends StatefulWidget {
  final ProductListLimit productListLimit;
  ComponentProductLimit(this.productListLimit);

  @override
  _ComponentProductLimitState createState() => _ComponentProductLimitState();
}

class _ComponentProductLimitState extends State<ComponentProductLimit> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      if (i == 'HKD') {
        _price = widget.productListLimit.productpricehk + ' ' + i;
      } else if (i == 'NTD') {
        _price = widget.productListLimit.productpricetw + ' ' + i;
      } else if (i == 'SGD') {
        _price = widget.productListLimit.productpricesg + ' ' + i;
      } else if (i == 'MYR') {
        _price = widget.productListLimit.productpricemy + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageProductDetail(
            idproduct: widget.productListLimit.idproducts,
            productLabel: widget.productListLimit.productname,
            productImage: Config().ipmain +
                widget.productListLimit.productfilepath +
                widget.productListLimit.productfilename,
            productPrice: _price,
            productPriceDiscount: '',
            productDescription: widget.productListLimit.description,
          ),
        ),
      ),
      child: Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //KONTEN GAMBAR
              Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CachedNetworkImage(
                      imageUrl: Config().ipmain +
                          widget.productListLimit.productfilepath +
                          widget.productListLimit.productfilename,
                      imageBuilder: (context, imageProvider) => Container(
                        height: 125,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          backgroundColor: Colors.orangeAccent,
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              //KONTEN NAMA
              Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Text(
                  widget.productListLimit.productname,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 5),
              //KONTEN HARGA
              Text(
                _price,
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
