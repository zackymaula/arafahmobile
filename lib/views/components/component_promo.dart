import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_promo.dart';
import 'package:arafahmobile/views/pages/page_product_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentPromo extends StatefulWidget {
  final PromoList promoList;
  ComponentPromo(this.promoList);

  @override
  _ComponentPromoState createState() => _ComponentPromoState();
}

class _ComponentPromoState extends State<ComponentPromo> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      if (i == 'HKD') {
        _price = widget.promoList.promopricehk + ' ' + i;
      } else if (i == 'NTD') {
        _price = widget.promoList.promopricetw + ' ' + i;
      } else if (i == 'SGD') {
        _price = widget.promoList.promopricesg + ' ' + i;
      } else if (i == 'MYR') {
        _price = widget.promoList.promopricemy + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageProductDetail(
            idpromo: widget.promoList.idpromo,
            productLabel: widget.promoList.promoname,
            productImage: Config().ipmain +
                widget.promoList.promofilepath +
                widget.promoList.promofilename,
            productPrice: _price,
            productPriceDiscount: '',
            productDescription: widget.promoList.promodescription,
          ),
        ),
      ),
      child: Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(Icons.beenhere, color: Colors.green),
                  ],
                ),
              ),
              //KONTEN GAMBAR
              Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CachedNetworkImage(
                      imageUrl: Config().ipmain +
                          widget.promoList.promofilepath +
                          widget.promoList.promofilename,
                      imageBuilder: (context, imageProvider) => Container(
                        height: 125,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          backgroundColor: Colors.orangeAccent,
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              //KONTEN NAMA
              Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Text(
                  widget.promoList.promoname,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 5),
              //KONTEN HARGA
              Text(
                _price,
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
