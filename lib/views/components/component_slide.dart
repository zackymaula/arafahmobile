import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_slide.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ComponentSlide extends StatelessWidget {
  final SlideList slidelist;
  ComponentSlide(this.slidelist);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: Config().ipmain + slidelist.filepath + slidelist.filename,
      imageBuilder: (context, imageProvider) => Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => Center(
        child: CircularProgressIndicator(
          strokeWidth: 2,
          backgroundColor: Colors.orangeAccent,
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
