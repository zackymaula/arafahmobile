import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_mg_catalog_category.dart';
import 'package:arafahmobile/views/pages/page_mg_catalog_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentMGCatalogCategory extends StatefulWidget {
  final MGCatalogCategory catalogCategory;
  ComponentMGCatalogCategory(this.catalogCategory);

  @override
  _ComponentMGCatalogCategoryState createState() =>
      _ComponentMGCatalogCategoryState();
}

class _ComponentMGCatalogCategoryState
    extends State<ComponentMGCatalogCategory> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      var price = int.parse(widget.catalogCategory.price);

      if (i == 'HKD') {
        _price = (price * 1).toString() + ' ' + i;
      } else if (i == 'NTD') {
        _price = (price * 4).toString() + ' ' + i;
      } else if (i == 'SGD') {
        _price = (price * 0.2).toString() + ' ' + i;
      } else if (i == 'MYR') {
        _price = (price * 0.6).toString() + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageMGCatalogDetail(
            id: widget.catalogCategory.id,
            name: widget.catalogCategory.name,
            sku: widget.catalogCategory.sku,
            price: widget.catalogCategory.price,
            description: widget.catalogCategory.description,
            image: widget.catalogCategory.image,
            categorynames: widget.catalogCategory.categorynames,
            categorysubnames: widget.catalogCategory.categorysubnames,
          ),
        ),
      ),
      child: Card(
        elevation: 2,
        child: Container(
          margin: EdgeInsets.all(3),
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl:
                        Config().mgpathimage + widget.catalogCategory.image,
                    imageBuilder: (context, imageProvider) => Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      height: MediaQuery.of(context).size.height / 4.9,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.orangeAccent,
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Text(
                widget.catalogCategory.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  fontFamily: "MadeTommySoft",
                ),
              ),
              SizedBox(height: 5),
              Text(
                _price,
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  fontFamily: "MadeTommySoft",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
