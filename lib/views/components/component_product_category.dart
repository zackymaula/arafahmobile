import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_product_category.dart';
import 'package:arafahmobile/views/pages/page_product_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentProductCategory extends StatefulWidget {
  final ProductListCategory productListCategory;
  ComponentProductCategory(this.productListCategory);

  @override
  _ComponentProductCategoryState createState() =>
      _ComponentProductCategoryState();
}

class _ComponentProductCategoryState extends State<ComponentProductCategory> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      if (i == 'HKD') {
        _price = widget.productListCategory.productpricehk + ' ' + i;
      } else if (i == 'NTD') {
        _price = widget.productListCategory.productpricetw + ' ' + i;
      } else if (i == 'SGD') {
        _price = widget.productListCategory.productpricesg + ' ' + i;
      } else if (i == 'MYR') {
        _price = widget.productListCategory.productpricemy + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => PageProductDetail(
            idproduct: widget.productListCategory.idproducts,
            productLabel: widget.productListCategory.productname,
            productImage: Config().ipmain +
                widget.productListCategory.productfilepath +
                widget.productListCategory.productfilename,
            productPrice: _price,
            productPriceDiscount: '',
            productDescription: widget.productListCategory.description,
          ),
        ),
      ),
      child: Card(
        elevation: 2,
        child: Container(
          margin: EdgeInsets.all(3),
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: Config().ipmain +
                        widget.productListCategory.productfilepath +
                        widget.productListCategory.productfilename,
                    imageBuilder: (context, imageProvider) => Container(
                      height: 125,
                      width: 125,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.orangeAccent,
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Text(
                widget.productListCategory.productname,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5),
              Text(
                //widget.productListCategory.productpricetw,
                _price,
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
