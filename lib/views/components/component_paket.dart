import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_paket.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ComponentPaket extends StatelessWidget {
  final PaketList paketList;
  ComponentPaket(this.paketList);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: Config().ipmain + paketList.filepath + paketList.filename,
      imageBuilder: (context, imageProvider) => Container(
        height: 300,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.contain,
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(1),
              blurRadius: 2,
              offset: Offset(1, 1),
            ),
          ],
        ),
      ),
      placeholder: (context, url) => Center(
        child: CircularProgressIndicator(
          strokeWidth: 2,
          backgroundColor: Colors.orangeAccent,
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
