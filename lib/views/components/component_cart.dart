import 'package:arafahmobile/blocs/b_cart.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_cart.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentCart extends StatelessWidget {
  final CartList cartList;
  ComponentCart(this.cartList);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC AUTH
        BlocProvider<BlocCartDelete>(
          builder: (context) => BlocCartDelete(),
        ),
      ],
      child: ComponentCartView(cartList),
    );
  }
}

class ComponentCartView extends StatefulWidget {
  final CartList cartList;
  ComponentCartView(this.cartList);

  @override
  _ComponentCartViewState createState() => _ComponentCartViewState();
}

class _ComponentCartViewState extends State<ComponentCartView> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      if (i == 'HKD') {
        _price = widget.cartList.productpricehk + ' ' + i;
      } else if (i == 'NTD') {
        _price = widget.cartList.productpricetw + ' ' + i;
      } else if (i == 'SGD') {
        _price = widget.cartList.productpricesg + ' ' + i;
      } else if (i == 'MYR') {
        _price = widget.cartList.productpricemy + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: CachedNetworkImage(
              imageUrl: Config().ipmain +
                  widget.cartList.productfilepath +
                  widget.cartList.productfilename,
              imageBuilder: (context, imageProvider) => Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              placeholder: (context, url) => Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.orangeAccent,
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Flexible(
            flex: 2,
            child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              width: MediaQuery.of(context).size.width / 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.cartList.productname,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    _price,
                    style: TextStyle(
                      fontSize: 17,
                      color: Colors.blue[800],
                      fontWeight: FontWeight.bold,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              //color: Colors.blue,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 70,
                    child: VerticalDivider(color: Colors.grey),
                  ),
                  IconButton(
                    icon: Icon(Icons.delete),
                    color: Colors.redAccent,
                    onPressed: () {
                      _showDialog(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showDialog(context) {
    BlocCartDelete bloccartdelete = BlocProvider.of<BlocCartDelete>(context);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 300,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Icon(
                    Icons.delete_forever,
                    color: Colors.redAccent,
                    size: 70,
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Yakin ingin menghapus produk ini dari keranjang?',
                    style: TextStyle(
                      fontSize: 25,
                      fontFamily: "MadeTommySoft",
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          'Hapus',
                          style: TextStyle(
                            color: Colors.redAccent,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          bloccartdelete
                              .dispatch(widget.cartList.idcart); //DELETE CART
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PageMain(selectedPage: 2),
                            ),
                            (Route<dynamic> route) => false,
                          );
                        },
                      ),
                      Spacer(),
                      FlatButton(
                        child: Text(
                          'Tidak',
                          style: TextStyle(
                            color: Colors.blue[800],
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
