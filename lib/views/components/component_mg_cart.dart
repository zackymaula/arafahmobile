import 'package:arafahmobile/blocs/b_mg_cart.dart';
import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_mg_cart.dart';
import 'package:arafahmobile/views/pages/page_main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ComponentMGCart extends StatelessWidget {
  final MGCartList cartList;
  ComponentMGCart(this.cartList);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BLOC CART DELETE
        // BlocProvider<BlocCartDelete>(
        //   builder: (context) => BlocCartDelete(),
        // ),
        BlocProvider<BlocMGCartDelete>(
          builder: (context) => BlocMGCartDelete(),
        ),
      ],
      child: ComponentMGCartView(cartList),
    );
  }
}

class ComponentMGCartView extends StatefulWidget {
  final MGCartList cartList;
  ComponentMGCartView(this.cartList);

  @override
  _ComponentMGCartViewState createState() => _ComponentMGCartViewState();
}

class _ComponentMGCartViewState extends State<ComponentMGCartView> {
  String _price = '';

  @override
  void initState() {
    _getPrefCurrency().then((i) {
      var price = int.parse(widget.cartList.price);

      if (i == 'HKD') {
        _price = (price * 1).toString() + ' ' + i;
      } else if (i == 'NTD') {
        _price = (price * 4).toString() + ' ' + i;
      } else if (i == 'SGD') {
        _price = (price * 0.2).toString() + ' ' + i;
      } else if (i == 'MYR') {
        _price = (price * 0.6).toString() + ' ' + i;
      }
      setState(() {});
    });

    super.initState();
  }

  Future<String> _getPrefCurrency() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(Config().prefcurrency) ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: CachedNetworkImage(
              imageUrl: widget.cartList.image,
              imageBuilder: (context, imageProvider) => Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              placeholder: (context, url) => Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.orangeAccent,
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Flexible(
            flex: 2,
            child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              width: MediaQuery.of(context).size.width / 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.cartList.name,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    _price,
                    style: TextStyle(
                      fontSize: 17,
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.bold,
                      fontFamily: "MadeTommySoft",
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              //color: Colors.blue,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 70,
                    child: VerticalDivider(color: Colors.grey),
                  ),
                  IconButton(
                    icon: Icon(Icons.delete),
                    color: Colors.redAccent,
                    onPressed: () {
                      _showDialog(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showDialog(context) {
    //BlocCartDelete bloccartdelete = BlocProvider.of<BlocCartDelete>(context);
    BlocMGCartDelete blocmgcartdelete =
        BlocProvider.of<BlocMGCartDelete>(context);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 300,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Icon(
                    Icons.delete_forever,
                    color: Colors.redAccent,
                    size: 70,
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Yakin ingin menghapus produk ini dari keranjang?',
                    style: TextStyle(
                      fontSize: 25,
                      fontFamily: "MadeTommySoft",
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          'Hapus',
                          style: TextStyle(
                            color: Colors.redAccent,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          blocmgcartdelete
                              .dispatch(widget.cartList.idcart); //DELETE CART
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PageMain(selectedPage: 2),
                            ),
                            (Route<dynamic> route) => false,
                          );
                        },
                      ),
                      Spacer(),
                      FlatButton(
                        child: Text(
                          'Tidak',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontFamily: "MadeTommySoft",
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
