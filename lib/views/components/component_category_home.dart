import 'package:arafahmobile/configs/config.dart';
import 'package:arafahmobile/models/m_category.dart';
import 'package:arafahmobile/views/pages/page_bangunan.dart';
import 'package:arafahmobile/views/pages/page_category_product_list.dart';
import 'package:arafahmobile/views/pages/page_paket.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ComponentCategoryHome extends StatelessWidget {
  final CategoryList categoryList;
  ComponentCategoryHome(this.categoryList);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => (categoryList.idcategories == 1) //PAKET ARAFAH
          ? Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => PagePaket(),
              ),
            )
          : (categoryList.idcategories == 7) //BAHAN BANGUNAN
              ? Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => PageBangunan(),
                  ),
                )
              : Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => PageCategoryProductList(
                      categoryId: categoryList.idcategories,
                      categoryName: categoryList.name,
                      categoryImage: Config().ipmain +
                          categoryList.filepath +
                          categoryList.filename,
                    ),
                  ),
                ),
      child: Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(5),
          child: Center(
            child: Column(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: Config().ipmain +
                      categoryList.filepath +
                      categoryList.filename,
                  imageBuilder: (context, imageProvider) => Container(
                    width: MediaQuery.of(context).size.width / 5,
                    height: MediaQuery.of(context).size.height / 9,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      backgroundColor: Colors.orangeAccent,
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  categoryList.name,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: "MadeTommySoft",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
