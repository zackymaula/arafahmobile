import 'package:arafahmobile/models/m_auth.dart';
import 'package:bloc/bloc.dart';

//INSERT DATA MEMBER | SETELAH INPUT OTP
class BlocAuthenticationRegister extends Bloc<String, AuthenticationRegister> {
  @override
  AuthenticationRegister get initialState =>
      UninitializedAuthenticationRegister();

  @override
  Stream<AuthenticationRegister> mapEventToState(String event) async* {
    try {
      AuthenticationRegister item =
          await AuthenticationRegister.postRegisterAuthFromAPI(event);
      yield item;
    } catch (_) {}
  }
}

//AMBIL DATA MEMBER | PAGE USER MAIN
class BlocAuthenticationGet extends Bloc<String, AuthenticationGet> {
  @override
  AuthenticationGet get initialState => UninitializedAuthenticationGet();

  @override
  Stream<AuthenticationGet> mapEventToState(String event) async* {
    try {
      AuthenticationGet item =
          await AuthenticationGet.postGetAuthFromAPI(event);
      yield item;
    } catch (_) {}
  }
}

//CHEKC DATA MEMBER | PAGE LOGIN INPUT NOMOR HP
class BlocAuthenticationCheck extends Bloc<String, AuthenticationCheck> {
  @override
  AuthenticationCheck get initialState => UninitializedAuthenticationCheck();

  @override
  Stream<AuthenticationCheck> mapEventToState(String event) async* {
    try {
      AuthenticationCheck item =
          await AuthenticationCheck.postCheckAuthFromAPI(event);
      yield item;
    } catch (_) {}
  }
}
