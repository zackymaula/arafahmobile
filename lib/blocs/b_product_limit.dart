import 'package:arafahmobile/models/m_product_limit.dart';
import 'package:bloc/bloc.dart';

class BlocProductListLimit extends Bloc<int, List<ProductListLimit>> {
  @override
  List<ProductListLimit> get initialState => List<UninitializedProductListLimit>();//[];

  @override
  Stream<List<ProductListLimit>> mapEventToState(int event) async* {
    try {
      List<ProductListLimit> itemlist = await ProductListLimit.getProductListLimitFromAPI();
      yield itemlist;
    } catch (_) {}
  }
}