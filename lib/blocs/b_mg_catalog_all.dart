import 'package:arafahmobile/models/m_mg_catalog_all.dart';
import 'package:bloc/bloc.dart';

class BlocMGCatalogAll extends Bloc<int, List<MGCatalogAll>> {
  @override
  List<MGCatalogAll> get initialState => List<UninitializedMGCatalogAll>();//[];

  @override
  Stream<List<MGCatalogAll>> mapEventToState(int event) async* {
    try {
      List<MGCatalogAll> itemlist = await MGCatalogAll.getCatalogAllFromApiMG();
      yield itemlist;
    } catch (_) {}
  }
}