import 'package:arafahmobile/models/m_mg_catalog_category.dart';
import 'package:bloc/bloc.dart';

class BlocMGCatalogCategory extends Bloc<String, List<MGCatalogCategory>> {
  @override
  List<MGCatalogCategory> get initialState =>
      List<UninitializedMGCatalogCategory>(); //[];

  @override
  Stream<List<MGCatalogCategory>> mapEventToState(String event) async* {
    try {
      List<MGCatalogCategory> itemlist =
          await MGCatalogCategory.getCatalogCategoryFromApiMG(event);
      yield itemlist;
    } catch (_) {}
  }
}
