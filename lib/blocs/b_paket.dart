import 'package:arafahmobile/models/m_paket.dart';
import 'package:bloc/bloc.dart';

class BlocPaketList extends Bloc<int, List<PaketList>> {
  @override
  List<PaketList> get initialState => List<UninitializedPaketList>();//[];

  @override
  Stream<List<PaketList>> mapEventToState(int event) async* {
    try {
      List<PaketList> itemlist = await PaketList.getPaketListFromAPI();
      yield itemlist;
    } catch (_) {}
  }
}