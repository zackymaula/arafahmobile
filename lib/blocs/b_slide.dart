import 'package:arafahmobile/models/m_slide.dart';
import 'package:bloc/bloc.dart';

class BlocSlideList extends Bloc<int, List<SlideList>> {
  @override
  List<SlideList> get initialState => List<UninitializedSlideList>();//[];

  @override
  Stream<List<SlideList>> mapEventToState(int event) async* {
    try {
      List<SlideList> itemlist = await SlideList.getSlideListFromAPI();
      yield itemlist;
    } catch (_) {}
  }
}