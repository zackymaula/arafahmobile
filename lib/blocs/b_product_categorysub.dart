import 'package:arafahmobile/models/m_product_categorysub.dart';
import 'package:bloc/bloc.dart';

class BlocProductListCategorySub extends Bloc<String, List<ProductListCategorySub>> {
  @override
  List<ProductListCategorySub> get initialState => List<UninitializedProductListCategorySub>();//[];

  @override
  Stream<List<ProductListCategorySub>> mapEventToState(String event) async* {
    try {
      List<ProductListCategorySub> itemlist = await ProductListCategorySub.getProductListCategorySubFromAPI(event);
      yield itemlist;
    } catch (_) {}
  }
}