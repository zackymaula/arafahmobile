import 'package:arafahmobile/models/m_category.dart';
import 'package:bloc/bloc.dart';

class BlocCategoryList extends Bloc<int, List<CategoryList>> {
  @override
  List<CategoryList> get initialState => List<UninitializedCategoryList>();//[];

  @override
  Stream<List<CategoryList>> mapEventToState(int event) async* {
    try {
      List<CategoryList> itemlist = await CategoryList.getCategoryListFromAPI();
      yield itemlist;
    } catch (_) {}
  }
}