import 'package:arafahmobile/models/m_mg_cart.dart';
import 'package:bloc/bloc.dart';

//BLOC CART LIST (MG)
class BlocMGCartList extends Bloc<String, List<MGCartList>> {
  @override
  List<MGCartList> get initialState => List<UninitializedMGCartList>(); //[];

  @override
  Stream<List<MGCartList>> mapEventToState(String event) async* {
    try {
      List<MGCartList> itemlist = await MGCartList.getCartListFromApi(event);
      yield itemlist;
    } catch (_) {}
  }
}

//BLOC CART INSERT (MG)
class BlocMGCartInsert extends Bloc<String, MGCartInsert> {
  @override
  MGCartInsert get initialState => UninitializedMGCartInsert();

  @override
  Stream<MGCartInsert> mapEventToState(String event) async* {
    try {
      MGCartInsert item = await MGCartInsert.postMGCartInsertAPI(event);
      yield item;
    } catch (_) {}
  }
}

//BLOC CART DELETE (MG)
class BlocMGCartDelete extends Bloc<String, MGCartDelete> {
  @override
  MGCartDelete get initialState => UninitializedMGCartDelete();

  @override
  Stream<MGCartDelete> mapEventToState(String event) async* {
    try {
      MGCartDelete item = await MGCartDelete.sendCartDeleteAPI(event);
      yield item;
    } catch (_) {}
  }
}
