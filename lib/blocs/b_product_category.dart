import 'package:arafahmobile/models/m_product_category.dart';
import 'package:bloc/bloc.dart';

class BlocProductListCategory extends Bloc<int, List<ProductListCategory>> {
  @override
  List<ProductListCategory> get initialState => List<UninitializedProductListCategory>();//[];

  @override
  Stream<List<ProductListCategory>> mapEventToState(int event) async* {
    try {
      List<ProductListCategory> itemlist = await ProductListCategory.getProductListCategoryFromAPI(event);
      yield itemlist;
    } catch (_) {}
  }
}