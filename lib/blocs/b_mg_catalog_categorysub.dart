import 'package:arafahmobile/models/m_mg_catalog_category.dart';
import 'package:arafahmobile/models/m_mg_catalog_categorysub.dart';
import 'package:bloc/bloc.dart';

class BlocMGCatalogCategorySub
    extends Bloc<String, List<MGCatalogCategorySub>> {
  @override
  List<MGCatalogCategorySub> get initialState =>
      List<UninitializedMGCatalogCategorySub>(); //[];

  @override
  Stream<List<MGCatalogCategorySub>> mapEventToState(String event) async* {
    try {
      List<MGCatalogCategorySub> itemlist =
          await MGCatalogCategorySub.getCatalogCategorySubFromApiMG(event);
      yield itemlist;
    } catch (_) {}
  }
}
