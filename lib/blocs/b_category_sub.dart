import 'package:arafahmobile/models/m_category_sub.dart';
import 'package:bloc/bloc.dart';

class BlocCategorySubList extends Bloc<int, List<CategorySubList>> {
  @override
  List<CategorySubList> get initialState => List<UninitializedCategorySubList>();//[];

  @override
  Stream<List<CategorySubList>> mapEventToState(int event) async* {
    try {
      List<CategorySubList> itemlist = await CategorySubList.getCategorySubListFromAPI(event);
      yield itemlist;
    } catch (_) {}
  }
}