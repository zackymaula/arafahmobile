import 'package:arafahmobile/models/m_promo.dart';
import 'package:bloc/bloc.dart';

class BlocPromoList extends Bloc<int, List<PromoList>> {
  @override
  List<PromoList> get initialState => List<UninitializedPromoList>();//[];

  @override
  Stream<List<PromoList>> mapEventToState(int event) async* {
    try {
      List<PromoList> itemlist = await PromoList.getPromoListFromAPI();
      yield itemlist;
    } catch (_) {}
  }
}