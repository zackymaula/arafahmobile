import 'package:arafahmobile/models/m_cart.dart';
import 'package:bloc/bloc.dart';

//BLOC CART LIST
class BlocCartList extends Bloc<String, List<CartList>> {
  @override
  List<CartList> get initialState => List<UninitializedCartList>();//[];

  @override
  Stream<List<CartList>> mapEventToState(String event) async* {
    try {
      List<CartList> itemlist = await CartList.getCartListFromAPI(event);
      yield itemlist;
    } catch (_) {}
  }
}

//BLOC CART INSERT
class BlocCartInsert extends Bloc<String, CartInsert> {
  @override
  CartInsert get initialState => UninitializedCartInsert();

  @override
  Stream<CartInsert> mapEventToState(String event) async* {
    try {
      CartInsert item = await CartInsert.postCartInsertAPI(event);
      yield item;
    } catch (_) {}
  }
}

//BLOC CART DELETE
class BlocCartDelete extends Bloc<String, CartDelete> {
  @override
  CartDelete get initialState => UninitializedCartDelete();

  @override
  Stream<CartDelete> mapEventToState(String event) async* {
    try {
      CartDelete item = await CartDelete.sendCartDeleteAPI(event);
      yield item;
    } catch (_) {}
  }
}
